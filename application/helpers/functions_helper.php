<?php
function my_url($parm){
	$parm = str_replace(" ","-",$parm);
	$parm = str_replace("'","",$parm);
	$parm = str_replace("\"","",$parm);
	$parm = str_replace("+","",$parm);
	$parm = str_replace("/","",$parm);
	$parm = str_replace("\\","",$parm);
	return $parm;
}

function retornaDateTimePTBR(){
	$h = "3"; //HORAS DO FUSO ((BRASÍLIA = -3) COLOCA-SE SEM O SINAL -).
	$hm = $h * 60;
	$ms = $hm * 60;
	//COLOCA-SE O SINAL DO FUSO ((BRASÍLIA = -3) SINAL -) ANTES DO ($ms). HORA
	$gmdatetime = gmdate("Y-m-d H:i", time()-($ms));

	return $gmdatetime;
}

?>