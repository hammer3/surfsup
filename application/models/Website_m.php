<?php
class Website_m extends CI_Model {

	public function is_login($check=''){
		if($this->session->has_userdata("mem_login")){
			if($data = $this->session->userdata("mem_login")){
				if(!empty($data->mem_id)){
					$this->getPlan();
					return true;
				}
			}
		}
		if($check=='header'){
			return false;
		}
		redirect(base_url('signin'));
	}
	public function getAPIkey(){
		return $this->db->get("api")->row();
	}
	public function getSurfAttribute($column){
		$this->db->select("DISTINCT({$column})");
		$this->db->where("sur_status","active");
		$this->db->from("surfboards");
		return $this->db->get();
	}
	public function rangeDate($date){
		if(empty($date)){
			return 'N/A';
		}
		if(strpos($date,",")){
			$exp = explode(",",$date);
			$startDate = date("M, d Y",trim($exp[0]));
			$endDate = date("M, d Y",trim(end($exp)));
			return "{$startDate} - {$endDate}";
		}else{
			return date("M, d Y",trim($date));
		}
	}
	
	public function CMS($id){
		return json_decode($this->db->get_where("web_setting",['web_page'=>$id])->row()->web_setting);
	}
	public function website(){
		return $this->db->get("website")->row();
	}
	public function checkAccounMissing(){
		if(null == $this->get_member_id()){
			return false;
		}
		$fields = [
			'mem_phone',
			'mem_country',
			'mem_city',
			'mem_zip',
			'mem_address',
		];
		
		$this->db->where("mem_id",$this->get_member_id());
		$data = json_decode(json_encode($this->db->get('members')->row()),true);
		foreach($fields as $key){
			if(empty($data[$key])){
				return true;
			}
		}
		return false;
	}
	public function checkEmail(){
		if(null == $this->get_member_id()){
			return false;
		}
		$fields = [
			'mem_email'
		];
		
		$this->db->where("mem_id",$this->get_member_id());
		$data = json_decode(json_encode($this->db->get('members')->row()),true);
		foreach($fields as $key){
			if(empty($data[$key])){
				return true;
			}
		}
		return false;
	}
	public function OtherCredits($getData=''){
		
		
		return 0;
	
	}
	public function getPlan(){
		$this->db->order_by("trx_id","DESC");
		$planId = $this->db->get_where("transaction",[
			"mem_id"=>$this->get_member_id(),
			"trx_status"=>"active"
		])->row();
		if(!empty($planId->pkg_id)){
			return $this->session->set_userdata("plan",$planId);
		}
		return false;
	}
	public function future_resrvation_header(){ //actually its for present header 
		$today = strtotime(date("Y-m-d"));
		$this->db->select("count(*) as count");
		//$this->db->select("* , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("surfboards as s");
		$this->db->join("location as l","s.sur_location = l.lcc_id");
		$this->db->join("location_category as lc","lc.loc_id = l.loc_id");
		$this->db->join("order as o","s.sur_id=o.sur_id");
		$this->db->where("o.mem_id",$this->get_member_id());
		$this->db->where("o.ord_surfboard_date >= ",$today);
		$this->db->order_by('o.ord_surfboard_date','ASC');
		$this->db->group_by('o.ord_number');
		$future = $this->db->get()->result_array();
		return count($future);
	}
	
	public function checkPlanExists(){
		$this->db->order_by("trx_id","DESC");
		$planId = $this->db->get_where("transaction",[
			"mem_id"=>$this->get_member_id(),
			"trx_status"=>"active"
		])->row();
		if(!empty($planId->pkg_id)){
			$this->db->where("pkg_id",$planId->pkg_id);
			$pkg = $this->db->get("packages")->row();
			if(!empty($pkg)){
				if($pkg->pkg_type=="PACKAGES"){
					$data = $this->performCalculation();
					return ($data['credits']-$data['used'])." créditos restantes";
				}else{
					return "Plano ativo: Assinatura1";
				}
			}else{
				return false;
			}
		}
		return false;
	}
	
	public function checkCreditsLeft(){
		$this->db->order_by("trx_id","DESC");
		$planId = $this->db->get_where("transaction",[
			"mem_id"=>$this->get_member_id(),
			"trx_status"=>"active"
		])->row();
		if(!empty($planId->pkg_id)){
			$this->db->where("pkg_id",$planId->pkg_id);
			$pkg = $this->db->get("packages")->row();
			if(!empty($pkg)){
				if($pkg->pkg_type=="PACKAGES"){
					$data = $this->performCalculation();
					return ($data['credits']-$data['used']);
				}else{
					return 0;
				}
			}else{
				return 0;
			}
		}
		return 0;
	}
	
	public function getPlanHeader(){
		$this->db->order_by("trx_id","DESC");
		$planId = $this->db->get_where("transaction",[
			"mem_id"=>$this->get_member_id(),
			"trx_status"=>"active"
		])->row();
		if(!empty($planId->pkg_id)){
			$this->db->where("pkg_id",$planId->pkg_id);
			$pkg = $this->db->get("packages")->row();
			if(!empty($pkg)){
				if($pkg->pkg_type=="PACKAGES"){
					$data = $this->performCalculation();
					$credit = ($data['credits']-$data['used']+$this->OtherCredits())." créditos restantes";
					if($credit>0){
						return $credit; 
					}else{
						return "0 créditos restantes";
					}
					
				}else{
					return "Plano Ativo: Assinatura " . $pkg->pkg_title;
				}
			}else{
				return "";
			}
		}
		return "";
	}
	public function performCalculation(){
		
		$return = [];
		$this->db->order_by("trx_id","DESC");
		$planId = $this->db->get_where("transaction",[
			"mem_id"=>$this->get_member_id(),
			"trx_status"=>"active"
		])->result_array();
		
		
		$return['pkg_id'] = $planId[0]['pkg_id'];
		$return['mem_id'] = $this->get_member_id();
		
		$pkg_type = $this->db->get_where("packages",["pkg_id"=>$return['pkg_id']])->row()->pkg_type;
		
		$pkg3 = array();
		$sn=0;
		foreach($planId as $pp){
			$this->db->where('pkg_id',$pp['pkg_id']);
			$pkg3[] = json_decode(json_encode($this->db->get("packages")->row()),true);
			$sn++;
		}
		
		
		$package = $pkg3;
		//die($this->db->last_query());
		//die(print_r($pkg3));
		///////////////////////////////////////////////////////////////////
		$this->db->where('mem_id',$this->get_member_id());
		$this->db->where("ord_type",$pkg_type);
		/*
		foreach($planId as $pp){
			$this->db->or_where('pkg_id',$pp['pkg_id']);
			$this->db->or_where('trx_id',$pp['trx_id']);
		}
		*/
		$order= $this->db->get("order")->result_array();
		//die($this->db->last_query());
		/////////////////////////////////////////////////////////////////////				 
		$order_dates = array_column($order,"ord_surfboard_date","ord_id");
		$alreadyBooked = array_count_values($order_dates);
		$currentDate = strtotime(date("Y-m-d"));
		
		$book_per_day = array_sum(array_column($package,'pkg_booking_per_day'));
		
		$isTodayExists = in_array($currentDate,$order_dates);
		//$checkAllDays = array_intersect($currentOrders,$order_dates);
		
		//$return['today']
		$return['allow_per_day'] = $book_per_day;
		$return['alreadyBooked'] = $alreadyBooked;
		//$return['toOrder'] = $checkAllDays;
		$return['type'] = $package[0]['pkg_type'];
		$return['credits'] = array_sum(array_column($package,'pkg_time'));
		$return['used'] = array_sum($alreadyBooked);
		//die(print_r($return));
		return $return;

	}
	public function showButton($plan=''){
		//default Button
		//$choosePlan = '<a href="'.base_url().'signup">Escolha o Plano<i class="fi-arrow-right"></i></a>';
		//if user Logged in the button stat will change
		//if($this->website_m->is_login("header")){
			$choosePlan = '<a href="'.base_url('plans/select'.$plan).'noplan">Escolha o plano <i class="fi-arrow-right"></i></a>';
		//}
		return $choosePlan;
	}
	public function get_surfboard_image($where='',$limit='',$orderby=''){
		if(!empty($where)){
			$this->db->where($where);
		}
		if(!empty($limit)){
			$this->db->limit($limit);
		}
		if(!empty($orderby)){
			$this->db->order_by($orderby);
		}
		 $data = $this->db->get("sur_images");
		 if(empty($data)){
			return false;
		 }
		//die(print_r($data));
		return $data;
	}
	public function get_surfboard($where='',$limit='',$orderby=''){
		
		if(!empty($where)){
			$this->db->where($where);
		}
		if(!empty($limit)){
			$this->db->limit($limit);
		}
		if(!empty($orderby)){
			$this->db->order_by($orderby);
		}		
		$this->db->select("*, st.sur_types as sur_type , s.sur_type as sst , sb.sur_brands as sur_brand , s.sur_brand as ssb");		
		$this->db->from("surfboards as s");
		$this->db->join("sur_type as st","st.type_id = s.sur_type");
		$this->db->join("sur_brand as sb","sb.brand_id = s.sur_brand");
		$this->db->join("location as l","s.sur_location = l.lcc_id");
		$this->db->join("location_category as lc","lc.loc_id = l.loc_id");
		return $this->db->get();
	}
	public function getAllSurfboard(){
		
		$this->db->select("*, st.sur_types as sur_type , s.sur_type as sst , sb.sur_brands as sur_brand , s.sur_brand as ssb");
		
		$this->db->from("surfboards as s");
		$this->db->join("sur_type as st","st.type_id = s.sur_type");
		$this->db->join("sur_brand as sb","sb.brand_id = s.sur_brand");
		$this->db->join("location as l","s.sur_location = l.lcc_id");
		$this->db->join("location_category as lc","lc.loc_id = l.loc_id");
		$data = $this->db->get()->result_array();
		return array_column($data,"sur_id");
	}
	public function getAllOrders($start,$end){ //get all orders
		$this->db->select('*');
		$this->db->from("order");
		$this->db->where("ord_surfboard_date >= $start AND ord_surfboard_date <= $end");
		$data = $this->db->get()->result_array();
		return array_column($data,"sur_id");
	}
	public function get_packages($where='',$limit='',$orderby=''){
		
		if(!empty($where)){
			$this->db->where($where);
		}
		if(!empty($limit)){
			$this->db->limit($limit);
		}
		if(!empty($orderby)){
			$this->db->order_by($orderby);
		}
		
		$this->db->select("*");
		$this->db->from("packages as p");
		
		return $this->db->get();
	}
	public function get_properties_by_id($id=''){
		if(empty($id)){
			return false;
		}
		return $this->db->get_where("packages_properties",['pkg_id'=>$id]);
	}
	public function get_sur_properties($id=''){
		if(empty($id)){
			return false;
		}
		return $this->db->get_where("surfboards_properties",['sur_id'=>$id]);
	}
	
	public function do_upload($index='')
	{
		if(empty($index)){
			return '';
		}
		$config['upload_path'] = UPLOAD_AVATAR;
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
		$config['max_size'] = '1024';
		$config['overwrite'] = TRUE;
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = TRUE;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload($index)) {
			return '';
		} else {
			$data  = $this->upload->data();
			return $data['file_name'];
		}
	}
	public function get_member_id(){
		if(!$this->session->has_userdata("mem_login")){
			return false;
		}
		return  $this->session->userdata("mem_login")->mem_id;
	}
	public function getFirstName(){
		if(!$this->session->has_userdata("mem_login")){
			return ucfirst("Guest");
		}
		return  ucfirst($this->session->userdata("mem_login")->mem_first);
	}
	public function get_member_image(){
		$user_id = $this->website_m->get_member_id();
		return $this->db->get_where("members",['mem_id'=>$user_id])->row()->mem_pic;
	}
	public function get_countries($where=''){
		if(!empty($where)){
			$this->db->where("c_id",$where);
		}
		return $this->db->get("countries");
	}
	public function do_login($data){
		$data['mem_password'] = $this->myadmin->doEncode($data['mem_password']);
		
		$row = $this->db->get_where("members",$data)->row();
		//die($row);
		if(!empty($row)){
			if(true){//if($row->mem_verify){ //enable verify email if you wish to
				$this->session->set_userdata("mem_login",$row);
				return true;
			}else{
				
				$this->session->set_flashdata('message_error', "Endereço de e-mail não verificado. Por favor verifique seu email");
				return true;
			}
		}
		$this->session->set_flashdata('message_error', "Login inválido");
		return false;	
		
	}
	public function formValidation($type="members"){
		$config  = [ 
            "members" => 
                    [
                    	[
                            'field' => 'mem_name',
                            'label' => 'Name', //firstname
                            'rules' => 'trim|required|min_length[3]|max_length[35]'
                    	],
            			[
                            'field' => 'mem_email',
                            'label' => 'Email Address', 
                            'rules' => 'trim|required|valid_email|callback_SignupEmailCheck'
                        ],
            			[
                            'field' => 'mem_phone',
                            'label' => 'número de telefone', 
                            'rules' => 'trim|required|min_length[7]'
                        ],
						[
                            'field' => 'mem_password',
                            'label' => 'password',
                            'rules' => 'required|min_length[6]'
                    	],
                    	[
                            'field' => 'mem_repassword',
                            'label' => 'Confirm Password',
                            'rules' => 'required|matches[mem_password]'
                    	]
					], //end member
			"signin"=>
				[
					[
						'field' => 'mem_email',
						'label' => 'Email Address', 
						'rules' => 'trim|required|valid_email'
					],
					[
						'field' => 'mem_password',
						'label' => 'password',
						'rules' => 'required|min_length[6]'
					]
				], //end sign in
			"account"=>
				[
					[
						'field' => 'mem_first',
						'label' => 'First Name', 
						'rules' => 'trim|required|min_length[3]|max_length[35]'
					],
					[
						'field' => 'mem_last',
						'label' => 'Last Name', 
						'rules' => 'trim|required|min_length[3]|max_length[35]'
					],
					
				], //end account
			"change_pswd"=>
				[
					[
						'field' => 'old_password',
						'label' => 'Old Password',
						'rules' => 'required|callback_check_old_password'
					],
					[
						'field' => 'new_password',
						'label' => 'New Password',
						'rules' => 'required|min_length[6]'
					],
					[
						'field' => 'renew_password',
						'label' => 'Confirm Password',
						'rules' => 'required|matches[new_password]'
					]
					
				], //end account
			"contact_us"=>
				[
					[
						'field' => 'cnt_name',
						'label' => 'Name', //firstname
						'rules' => 'trim|required|min_length[3]|max_length[35]'
					],
					[
						'field' => 'cnt_email',
						'label' => 'Email Address', 
						'rules' => 'trim|required|valid_email'
					],
					[
						'field' => 'cnt_message',
						'label' => 'Message', 
						'rules' => 'required'
					],
					[
						'field' => 'cnt_subject',
						'label' => 'Subject', 
						'rules' => 'trim|required'
					]
					
				]
			];
		return $config[$type];	
	}
	private function sendEmail($to,$message){
		
	}
   
}
?>
