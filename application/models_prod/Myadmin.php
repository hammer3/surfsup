<?php
class Myadmin extends CI_Model {

	public function doEncode($passwd){
		return sha1('H@pAs!d.'.$passwd);
	}
	public function do_upload($index='')
	{
		if(empty($index)){
			return false;
		}
		$config['upload_path'] = UPLOAD_PATH."website/";
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|svg';
		$config['max_size'] = '1024';
		$config['overwrite'] = TRUE;
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = TRUE;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload($index)) {
			return false;
		} else {
			$data  = $this->upload->data();
			return $data['file_name'];
		}
	}
    public function do_login(){
		$var  = $this->input->post();
		$users = $this->db->get_where("users",[
				"usr_email"=>$var['email'],
				"usr_password"=>$this->doEncode($var['password'])
		])->row();
		if(!empty($users)){
			if($users->usr_email == $var['email'] AND $users->usr_password== $this->doEncode($var['password'])){
				$data = [
						"login" => "yes",
						"admin" =>$users->usr_type=="Administrator"?"yes":"no",
						"location"=>$users->lcc_id,
						"id"=>$users->usr_id
						];
				$this->session->set_userdata("admin",$data);
				$this->success("Successfully Logged In");
				return true;
			}
		}
		return false;
	}
	public function is_admin($header=''){
		if($this->session->has_userdata("admin")){
			
			if($data = $this->session->userdata("admin")){
				if($data['admin']=='yes'){
					return true;
				}else{
					if(!empty($header)){
						if($header=='location'){
							return $data['location'];
						}
						if($header=='id'){
							return $data['id'];
						}
					}
				}
			}
		}
		return false;
	}
	public function is_login(){
		if($this->session->has_userdata("admin")){
			if($data = $this->session->userdata("admin")){
				if($data['login']=='yes'){
					return true;
				}
			}
		}
		redirect(base_url("admin"));
	}
	
	public function get_location($where=''){
		if(!empty($where)){
			$this->db->where($where);
		}else{		
			$this->db->where(['lcc_status'=>'active']);
		}
		return $this->db->get("location");
	}
	public function get_location_category($where=''){
		if(!empty($where)){
			$this->db->where($where);
		}else{		
			$this->db->where(['loc_status'=>'active']);
		}
		return $this->db->get("location_category");
	}
	
	//**Set Flash Message**//
	public function success($message){
		$message = empty($message)?'Its Done':$message;
		$this->session->set_flashdata('success_message', $message);
	}
	public function error($message){
		$message = empty($message)?'Its Done':$message;
		$this->session->set_flashdata('error_message', $message);
	}
	function sendMail($to='',$subject,$message,$from='')
	{
		if(empty($from)){
			$from = $this->website_m->website()->site_noreply_email;
		}
		if(empty($to)){
			$this->website_m->website()->site_email;
		}
		$this->load->library('email');
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->from($from); // change it to yours
		$this->email->to($to);// change it to yours
		$this->email->subject($subject);
		$this->email->message($message);
		
		if($this->email->send())
		{
			//print_r($this->email->print_debugger());
			return true;
		}
		else
		{
			//print_r($this->email->print_debugger());
			return false;
		}

	}
}
?>