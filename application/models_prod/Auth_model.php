<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function check_authentication($username, $admin_password) {
        $this->db->select('*');
        $this->db->where(array('username' => $username, 'password' => md5($admin_password)));
        $result = $this->db->get('admin')->result_array();

        if (!empty($result)) {
            if ($result[0]['username'] == $username && $result[0]['password'] == md5($admin_password)) {
                return $result;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
	public function is_login(){
		if($this->session->has_userdata("mem_id")){
			return $this->db->get_where("members",["mem_id"=>$this->session->userdata("mem_id")])->row();
		}else{
			return false;
		}
	}

}
