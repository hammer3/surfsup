<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {
	public $data;
	public function __construct(){		
		parent::__construct();
		$this->website_m->is_login();
	}
	public function index()
	{
		$user_id = $this->website_m->get_member_id();
		if($data = $this->input->post()){
			if(!$this->website_m->checkEmail()){
				unset($data['mem_email']); //don't change Email address
				
			}
			$upload = $this->website_m->do_upload('profile_pic');
			if(!empty($upload)){
				@unlink(UPLOAD_AVATAR.$this->website_m->get_member_image()); //remove Prevoius Image
				$data['mem_pic'] = $upload;
			}
			$this->db->where("mem_id",$user_id);
			$this->db->update("members",$data);
			$this->session->set_flashdata('message_success', "atualizado com sucesso ");
			//after 2sec showing message of successfully updated , redirect to browse surfboards'
			if($this->session->flashdata('redirect')){
				$this->output->set_header('refresh:2; url='.base_url("packages"));
			}
			elseif($this->session->has_userdata("planid")){
				$this->output->set_header('refresh:2; url='.base_url("payment"));
			}elseif($this->session->has_userdata("postData")){
				$this->output->set_header('refresh:2; url='.base_url("reservation/reserveNow"));
			}else{		
				$this->output->set_header('refresh:2; url='.base_url("browse-surfboards"));
			}
			//redirect("browse-surfboards");
		}
		$this->data['user'] = $this->db->get_where("members",['mem_id'=>$user_id])->row();
		$this->data['page'] = 'account';
		$this->load->view('website/my-account',$this->data);

	}
	public function changepaswd(){
		
		$this->form_validation->set_rules($this->website_m->formValidation("change_pswd"));
		$user_id = $this->website_m->get_member_id();
		if($this->form_validation->run()){
			if($var = $this->input->post()){
				$password = $this->myadmin->doEncode($var['new_password']);
				$this->db->set("mem_password",$password);
				$this->db->where("mem_id",$user_id);
				$this->db->update("members");
				$this->session->set_flashdata('message_success_pwd', "Nova senha atualizada com sucesso");
			}
		}
		if(validation_errors()){
			$this->session->set_flashdata('message_error_pwd', validation_errors());
		}
		redirect("account");
	}
	public function  check_old_password($password){
		/*
			check old password callback form validation array in website_m

		*/
		$password = $this->myadmin->doEncode($password);
		$result = $this->db->get_where('members',['mem_password'=>$password])->row();
		if(!empty($result)){
			if($result->mem_password==$password){
				return true;
			}
		}
		$this->form_validation->set_message('check_old_password', "Senha antiga inválida.");
		return false;
    }
}