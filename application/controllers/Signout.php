<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signout extends CI_Controller {
	public $data;
	public function __construct(){		
		parent::__construct();
		
	}
	
	public function index(){
		setcookie("first","done",time()-12*60*60);
		unset($_SESSION);
		session_destroy();
		redirect(base_url());
	}

}