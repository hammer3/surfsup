<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	public $data;
	
	public function __construct(){		
		parent::__construct();
		$this->myadmin->is_login();
		if(!$this->myadmin->is_admin()){
			redirect(ADMIN);
		}
		$this->data['users_active'] = true;
	}
	/*Show Login Page*/
	//usr_id	usr_email	usr_password	usr_type	usr_users	usr_status

	public function index()
	{
		$this->data['row'] = $this->db
							->select('*')
							->from('users as u')
							->join("location as l"," l.lcc_id=u.lcc_id ")
							->get();
		$this->data['page'] = 'users/users';
		
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	public function add($id=''){
		if(!empty($id)){
			$where = ['usr_id'=>$id];
			$this->data['row'] = $this->db->get_where("users",$where)->row();
		}
		if($this->input->post()){
			
			$data = $this->input->post();
			$data['usr_status'] = isset($data['usr_status'])?'active':'inactive';
			if(empty($data['usr_password'])){
					unset($data['usr_password']);
			}else{
				$mypwd = $data['usr_password'];
				$data['usr_password'] = $this->myadmin->doEncode($data['usr_password']);
			}
			if(!empty($id)){
				$this->db->where($where);
				$this->db->set($data);
				$this->db->update("users");
				$this->myadmin->success("User Successfully Updated");
				redirect(ADMIN.'/users/');
			}
			$check = $this->data['row'] = $this->db->get_where("users",["usr_email"=>$data['usr_email']])->row();
			if(!empty($check)){
				$this->myadmin->success("Error While Saving");
				redirect(ADMIN.'/users/');
			}
			if($this->db->insert("users",$data) ){
				$this->myadmin->sendMail($data['usr_email'],'User Successfully Added to Surfboards',"
					Email : {$data['usr_email']}
					Password: {$mypwd}
				");
				$this->myadmin->success("User Successfully Saved");
				redirect(ADMIN.'/users/');
			}else{
				$this->myadmin->success("Error While Saving");
			}
		}
		
		$this->data['get_category'] = $this->db->get("location");
		$this->data['page'] = 'users/add-users';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	public function delete($id)
	{
		if(!empty($id)){
			$this->db->where('usr_id',$id);
			if($this->db->delete("users")){
				$this->myadmin->success("Successfully Deleted");
				redirect(ADMIN.'/users/');
			}
		}
		$this->myadmin->error("Error While Deleting");
		redirect(ADMIN.'/users/');
	}

	
}
