<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public $data;
	
	public function __construct(){		
		parent::__construct();
		$this->myadmin->is_login();
		if(!$this->myadmin->is_admin()){
			redirect(ADMIN);
		}
		$this->data['manage_active'] = true;
	}
	///**Mange Home page**///
	public function index(){
		if($data = $this->input->post()){
			
			$this->db->where('web_page','home');
			$row = $this->db->get("web_setting")->row()->web_setting;
			
			foreach($_FILES as $key=>$array){
				if($upload = $this->myadmin->do_upload($key)){
					if(!empty($upload)){
						$data[$key] = $upload;
					}
				}
			}
			
			$mydata = array_diff(json_decode($row,true),$data);
			$data = array_merge($mydata,$data);
			
			$this->db->set('web_setting',json_encode($data));
			$this->db->where('web_page','home');
			$this->db->update("web_setting");
			
			
			
		}
		$this->db->where('web_page','home');
		$row = $this->db->get("web_setting")->row()->web_setting;
		$this->data['row'] = json_decode($row);
		$this->data['page'] = 'pages/home';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function contact(){
		if($data = $this->input->post()){
			
			
			$this->db->set('web_setting',json_encode($data));
			$this->db->where('web_page','contact');
			$this->db->update("web_setting");
			
			
			
		}
		$this->db->where('web_page','contact');
		$row = $this->db->get("web_setting")->row()->web_setting;
		$this->data['row'] = json_decode($row);
		$this->data['page'] = 'pages/contact';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function tos(){
		if($data = $this->input->post()){
			$this->db->set('web_setting',json_encode($data));
			$this->db->where('web_page','tos');
			$this->db->update("web_setting");
		}
		$this->db->where('web_page','tos');
		$row = $this->db->get("web_setting")->row()->web_setting;
		$this->data['row'] = json_decode($row);
		$this->data['page'] = 'pages/tos';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function pp(){
		if($data = $this->input->post()){
			
			
			$this->db->set('web_setting',json_encode($data));
			$this->db->where('web_page','pp');
			$this->db->update("web_setting");
			
			
			
		}
		$this->db->where('web_page','pp');
		$row = $this->db->get("web_setting")->row()->web_setting;
		$this->data['row'] = json_decode($row);
		$this->data['page'] = 'pages/pp';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	//why page//
	public function how(){
		if($data = $this->input->post()){
			
			$this->db->where('web_page','how');
			$row = $this->db->get("web_setting")->row()->web_setting;			
			
			foreach($_FILES as $key=>$array){
				if($upload = $this->myadmin->do_upload($key)){
					if(!empty($upload)){
						$data[$key] = $upload;
					}
				}
			}
			
			$mydata = array_diff(json_decode($row,true),$data);
			$data = array_merge($mydata,$data);			
			
			$this->db->set('web_setting',json_encode($data));
			$this->db->where('web_page','how');
			$this->db->update("web_setting");
		}
		$this->db->where('web_page','how');
		$row = $this->db->get("web_setting")->row()->web_setting;
		$this->data['row'] = json_decode($row);
		$this->data['page'] = 'pages/how';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function buttons(){
		if($data = $this->input->post()){
			
			$this->db->where('web_page','buttons');
			$row = $this->db->get("web_setting")->row()->web_setting;			
			
			foreach($_FILES as $key=>$array){
				if($upload = $this->myadmin->do_upload($key)){
					if(!empty($upload)){
						$data[$key] = $upload;
					}
				}
			}
			
			$mydata = array_diff(json_decode($row,true),$data);
			$data = array_merge($mydata,$data);			
			
			$this->db->set('web_setting',json_encode($data));
			$this->db->where('web_page','buttons');
			$this->db->update("web_setting");
		}
		$this->db->where('web_page','buttons');
		$row = $this->db->get("web_setting")->row()->web_setting;
		$this->data['row'] = json_decode($row);
		$this->data['page'] = 'pages/buttons';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	///**Mange About us page**///
	public function about_us(){
		if($data = $this->input->post()){
			
			$this->db->where('web_page','about');
			$row = $this->db->get("web_setting")->row()->web_setting;			
			
			foreach($_FILES as $key=>$array){
				if($upload = $this->myadmin->do_upload($key)){
					if(!empty($upload)){
						$data[$key] = $upload;
					}
				}
			}
			
			$mydata = array_diff(json_decode($row,true),$data);
			$data = array_merge($mydata,$data);			
			
			$this->db->set('web_setting',json_encode($data));
			$this->db->where('web_page','about');
			$this->db->update("web_setting");
		}
		$this->db->where('web_page','about');
		$row = @$this->db->get("web_setting")->row()->web_setting;
		$this->data['row'] = json_decode($row);
		$this->data['page'] = 'pages/about';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
//help desk
	public function help(){
		
		$this->data['page'] = 'pages/help';
		$this->data['row'] = $this->db->get("help");
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function add_help($id = '') //help
    {
		if($data = $this->input->post()){
			$data['help_status'] = isset($data['help_status'])?'active':'inactive';
			if($image = $this->myadmin->do_upload('image')){
				$data['help_image'] = $image;
			}
			$this->db->set($data);
			if(empty($id)){
				$this->db->insert("help");
			}else{
				$this->db->where("help_id",$id);
				$this->db->update("help");
			}
			redirect(ADMIN."/home/help");
		}
		if(!empty($id)){
			$this->db->where("help_id",$id);
			$this->data['row'] = $this->db->get("help")->row();
		}
		
        $this->data['page'] = 'pages/add-help';
		$this->load->view('admin/include/sitemaster',$this->data);
    }
	public function delete_help($id = 0){
		if(!empty($id)){
			$this->db->where("help_id",$id);
			$this->db->delete("help");
		}
		redirect(ADMIN."/home/help");
	}		
/*Home Page Slider*/
	public function whyPage(){
		if($data = $this->input->post()){
			$this->db->set('web_setting',json_encode($data));
			$this->db->where('web_page','why');
			$this->db->update("web_setting");			
		}
		$this->db->where('web_page','why');
		$row = $this->db->get("web_setting")->row()->web_setting;
		$this->data['row'] = json_decode($row);
		$this->data['page'] = 'pages/why-joinweb';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function why()
	{
		
		$this->data['page'] = 'pages/view-why-join';
		$this->db->order_by("why_id","DESC");
		$this->data['row'] = $this->db->get("why");
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function add_why($id = '') //slider
    {
		if($data = $this->input->post()){
			$data['why_status'] = isset($data['why_status'])?'active':'inactive';
			if($image = $this->myadmin->do_upload('image')){
				$data['why_image'] = $image;
			}
			$this->db->set($data);
			if(empty($id)){
				$this->db->insert("why");
			}else{
				$this->db->where("why_id",$id);
				$this->db->update("why");
			}
			redirect(ADMIN."/home/why");
		}
		if(!empty($id)){
			$this->db->where("why_id",$id);
			$this->data['row'] = $this->db->get("why")->row();
		}
		
        $this->data['page'] = 'pages/why-join';
		$this->load->view('admin/include/sitemaster',$this->data);
    }
	public function delete_why($id = 0){
		if(!empty($id)){
			$this->db->where("why_id",$id);
			$this->db->delete("why");
		}
		redirect(ADMIN."/home/why");
	}	
	/*Home Page Slider*/
	public function slider()
	{
		
		$this->data['page'] = 'pages/view-slider';
		$this->data['row'] = $this->db->get("slider");
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function add_slider($id = '') //slider
    {
		if($data = $this->input->post()){
			$data['slider_status'] = isset($data['slider_status'])?'active':'inactive';
			if($image = $this->myadmin->do_upload('image')){
				$data['slider_image'] = $image;
			}
			$this->db->set($data);
			if(empty($id)){
				$this->db->insert("slider");
			}else{
				$this->db->where("slider_id",$id);
				$this->db->update("slider");
			}
			redirect(ADMIN."/home/slider");
		}
		if(!empty($id)){
			$this->db->where("slider_id",$id);
			$this->data['row'] = $this->db->get("slider")->row();
		}
		
        $this->data['page'] = 'pages/add-slider';
		$this->load->view('admin/include/sitemaster',$this->data);
    }
	public function delete_slider($id = 0){
		if(!empty($id)){
			$this->db->where("slider_id",$id);
			$this->db->delete("slider");
		}
		redirect(ADMIN."/home/slider");
	}
	/*
	public function website()
	{
		if($data = $this->input->post()){
			$this->db->set($data);
			$this->db->update("website");
		}
		$this->data['row'] = $this->db->get("website")->row();
		$this->data['page'] = 'pages/website-setting';
		$this->load->view('admin/include/sitemaster',$this->data);
	}*/
	public function people()
	{
		
		$this->data['page'] = 'pages/view-about-people';
		$this->db->order_by("people_id","DESC");
		$this->data['row'] = $this->db->get("people");
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function add_people($id = '') //slider
    {
		if($data = $this->input->post()){
			$data['people_status'] = isset($data['people_status'])?'active':'inactive';
			if($image = $this->myadmin->do_upload('image')){
				$data['people_image'] = $image;
			}
			$this->db->set($data);
			if(empty($id)){
				$this->db->insert("people");
			}else{
				$this->db->where("people_id",$id);
				$this->db->update("people");
			}
			redirect(ADMIN."/home/people");
		}
		if(!empty($id)){
			$this->db->where("people_id",$id);
			$this->data['row'] = $this->db->get("people")->row();
		}
		
        $this->data['page'] = 'pages/about-people';
		$this->load->view('admin/include/sitemaster',$this->data);
    }
	public function delete_people($id = 0){
		if(!empty($id)){
			$this->db->where("people_id",$id);
			$this->db->delete("people");
		}
		redirect(ADMIN."/home/people");
	}	
	
}
