<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public $data;
	
	public function __construct(){		
		parent::__construct();
		$this->myadmin->is_login();
	}
	/*Show Login Page*/
	public function dashboard()
	{
		$this->data['count_members'] = $this->db->get("members")->result_array();
		$this->data['count_contact'] = $this->db->get("contact")->result_array();
		
		
		$this->data['count_issued'] = $this->db->get_where("order",["ord_status"=>"issued"])->result_array();
		
		///////////////////////////////////////////////////////////
		$this->db->select("* , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("order as o");
		$this->db->join("surfboards as s","s.sur_id=o.sur_id");
		$this->db->join("packages as p","p.pkg_id=o.pkg_id");
		$this->db->join("location as l","l.lcc_id=s.sur_location");
		$this->db->where("o.ord_end <",time());
		$this->db->where("o.ord_status",'issued');
		$this->db->group_by('o.ord_number');
		$get = $this->db->get();
		$this->data['count_delayed'] = $get->result_array();
		
		////////////////////////////////////////////////////////////
		
		
		$this->data['count_today'] = $this->db->select('*')
											 ->from('order')
											 ->where("ord_surfboard_date ",strtotime(date("Y-m-d")))
											 ->get()
											 ->result_array();
		
		//$this->data['count_contact'] = $this->db->get("contact")->result_array();
		//$this->data['count_contact'] = $this->db->get("contact")->result_array();
		
		$this->data['page'] = 'dashboard';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function website()
	{
		$this->data['manage_active'] = true;
		if($data = $this->input->post()){
			$this->db->set($data);
			$this->db->update("website");
		}
		$this->data['row'] = $this->db->get("website")->row();
		$this->data['page'] = 'pages/website-setting';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	
}
