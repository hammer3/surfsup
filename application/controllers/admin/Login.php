<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public $data;
	/*Show Login Page*/
	public function index()
	{

		if($this->input->post()){
			if($this->myadmin->do_login()){
				redirect(base_url(ADMIN.'/admin/dashboard'));
			}else{
				$this->data['message'] = "Invalid Login";
			}
		}
		$this->data['page'] = 'login';
		$this->load->view('admin/login',$this->data);
	}

	public function newpaswd($id=''){
		
		if(!empty($id)){
			$this->db->where("reset_hash",$id);
			$sam = $data = $this->db->get("reset")->row();
			if($data2 = $this->input->post()){
				$data = $data2;
				if(md5($data['newpassword'])==md5($data['renewpassword']) AND !empty($data['renewpassword'])){
					$this->db->where("usr_id",$sam->mem_id);
					$this->db->set("usr_password",$this->myadmin->doEncode($data['newpassword']));
					$this->db->update("users");
					
					$this->db->where(["reset_hash"=>$id]);
					$this->db->set("reset_status","inactive");
					$this->db->update("reset");
				
					redirect(ADMIN);
				}
			}
			
			if(!empty($data)){
				$this->db->where("usr_id",$data->mem_id);
				$row = $this->db->get("users")->row();
				
			}else{
				die("<h1>Invalid Link or Expired Link</h1>");
			}
		}else{
			die("<h1>Invalid Link or Expired Links</h1>");
		}
		$this->data['page'] = 'newpasswod';
		$this->load->view("admin/newpassword",$this->data);

	}
	public function forget($message=''){
		$this->data['page'] = 'login';
		if($this->input->post()){
			$data = $this->db->get_where("users",["usr_email"=>$this->input->post("email")])->row();
			if(!empty($data)){
				$randomHash = sha1(rand(0,9999).time().'d@It');
				$this->db->insert("reset",[
					'reset_hash'=>$randomHash,
					'mem_id'=>$data->usr_id
				]);
				//$data->usr_email;
				$this->myadmin->sendMail($data->usr_email,"Reset Password Surfboard's Admin","
				<br>
				<h1>Reset Password</h1>
				<h3><a href='".base_url(ADMIN)."/newpaswd/{$randomHash}'>Click Here to reset Your password</a></h3>
				
				");
				$this->data['message'] = "An Email has been sent to your email address including Reset Instructions.";
				
			}else{
				$this->data['message'] = "Invalid Email Address";
			}
		}
		$this->load->view('admin/forget',$this->data);
	}
}
