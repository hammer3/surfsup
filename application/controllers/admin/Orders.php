<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {
	public $data;
	public $today;
	public $where_l;
	public $where;
	public function __construct(){		
		parent::__construct();	
		$this->myadmin->is_login();
		$this->today = strtotime(date("Y-m-d"));
		if($location = $this->myadmin->is_admin('location')){
			$this->where_l = ['l.lcc_id'=>$location];
			$this->where = ['lcc_id'=>$location];
		}
		$this->data['reservations_active'] = true;
		
	}
	/*Show Login Page*/
	public function index()
	{

		$this->data['heading'] = 'Todas as Reservas';
		$this->db->select(" * , GROUP_CONCAT(o.ord_surfboard_date SEPARATOR ',') as range_date ");
		$this->db->from("order as o");
		$this->db->join("surfboards as s","s.sur_id=o.sur_id");
		$this->db->join("members as m","m.mem_id=o.mem_id");
		$this->db->join("packages as p","p.pkg_id=o.pkg_id");
		$this->db->join("location as l","l.lcc_id=s.sur_location");
		$this->db->group_by('o.ord_number');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where_l);
		}
		$this->db->order_by('o.ord_id', 'desc');
		$get = $this->db->get();
		$this->data['row'] = $get;
		
		//echo $this->db->last_query();
		
		$this->data['page'] = 'orders/all';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function detalhe()
	{
		$this->data['heading'] = 'Gerenciar ações das reservas ';
		 $this->db->select("*");

		$this->db->from("VIEW_ALL_ORDER_RESERVATION");

		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$this->db->order_by('ord_id', 'DESC');
		$get = $this->db->get();
		$this->data['row'] = $get;
		
		//echo $this->db->last_query();
		
		$this->data['page'] = 'orders/detalhe';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function delayedSurf()
	{
		$this->data['heading'] = 'Surfboard\'s com Pagamentos Atrasados';
		$this->db->select(" * ");
		$this->db->from("VIEW_ALL_ORDER_RESERVATION");
		$this->db->where("status_atual",'issued');
		$this->db->where("ord_end <=",$this->today-86400);		
		$this->db->group_by('ord_number');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$get = $this->db->get();
		$this->data['row'] = $get;
		//echo $get->row()->ord_end;
		//print_r($this->db->last_query());
		//exit;
		$this->data['page'] = 'orders/delayed';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function today()
	{
		$this->data['heading'] = 'Reservas de hoje';
		$this->db->select(" * ");
		$this->db->from("VIEW_ALL_ORDER_RESERVATION");
		$this->db->where("status_atual",'pending');
		$this->db->where("ord_start =",$this->today);
		$this->db->group_by('ord_number');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$get = $this->db->get();
		$this->data['row'] = $get;
		//die(print_r($get->result_array()));
		
		$this->data['page'] = 'orders/orders';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function issued()
	{
		$this->data['heading'] = 'Pranchas emitidas';
		$this->db->select("*");
		$this->db->from("VIEW_ALL_ORDER_RESERVATION");
		//$this->db->where("ord_start >= ",$this->today);
		//$this->db->where("ord_end <= ",$this->today);		
		$this->db->where("status_atual",'issued');
		$this->db->group_by('ord_number');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$get = $this->db->get();
		$this->data['row'] = $get;
		//die(print_r($get->result_array()));
		
		$this->data['page'] = 'orders/issued';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function completed()
	{
		$this->data['heading'] = 'Reservas Concluídas';
		$this->db->select(" * ");
		$this->db->from("VIEW_ALL_ORDER_RESERVATION");
		$this->db->where("status_atual",'completed');
		$this->db->group_by('ord_number');
		$this->db->order_by('ord_id', 'desc');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}

		$get = $this->db->get();
		$this->data['row'] = $get;
		//echo '<pre>';
		//echo $this->db->last_query();
		//echo '<pre>';
		
		//die(print_r($get->result_array()));
		
		$this->data['page'] = 'orders/completed';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function canceled()
	{
		$this->data['heading'] = 'Pedidos Cancelados';
		$this->db->select(" * ");
		$this->db->from("VIEW_ALL_ORDER_RESERVATION");
		$this->db->where("status_atual",'canceled');
		$this->db->group_by('ord_number');
		$this->db->order_by('ord_id', 'DESC');
		if(!$this->myadmin->is_admin()){
			$this->db->where($this->where);
		}
		$get = $this->db->get();
		$this->data['row'] = $get;
		
		$this->data['page'] = 'orders/completed';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	/*
	funcao alterada em 06-01-2020
	inserido novo parametro onde é enviado o ord_id e o ord_number.
	Na versão anterior era inserido na tabela de order_status na coluna de ord_id o ord_number e agora é salvo o id da tabela order
	e tbm criado a change_date que guardara a data da inserção de novo status
	*/
	public function issue($id='null',$id_order=""){

		$id = ($id == 'null') ? $this->input->post()['ord_number_selecionado']!=""?$this->input->post()['ord_number_selecionado']:"" : $id;

		if(empty($id)){
			redirect(ADMIN."/orders/today");
		}
		$this->db->set("ord_status","issued");
		$this->db->where("ord_number",$id);//numero randomico criado atravez da data e horario da reserva
		$this->db->update("order");
		$this->db->insert("order_status",[
			"ord_id"=>$id,
			"ord_cod"=>($id_order == '' ? $this->input->post()['ord_id_selecionado'] : $id_order),
			"change_date"=>time(),
			"usr_id"=>$this->myadmin->get_id(),
			"ors_status"=>"issued",
			"ors_nome_func"=>$this->input->post()['nome_funcionario'] != '' ? $this->input->post()['nome_funcionario'] : "",
			"ors_observation"=>$this->input->post()['observacao'] != '' ? $this->input->post()['observacao'] : ""
		]);
		redirect(ADMIN."/orders/today");
	}
	/*
	funcao alterada em 06-01-2020
	inserido novo parametro onde é enviado o ord_id e o ord_number.
	Na versão anterior era inserido na tabela de order_status na coluna de ord_id o ord_number e agora é salvo o id da tabela order
	e tbm criado a change_date que guardara a data da inserção de novo status
	*/
	public function cancel($id='',$id_order=""){
		if(empty($id)){
			redirect(ADMIN."/orders/today");
		}
		$this->db->set("ord_status","canceled");
		$this->db->where("ord_number",$id);
		$this->db->update("order");
		$this->db->insert("order_status",[
			"ord_id"=>$id,
			"ord_cod"=>$id_order,
			"change_date"=>time(),
			"usr_id"=>$this->myadmin->get_id(),
			"ors_status"=>"canceled"
		]);
		redirect(ADMIN."/orders/today");
	}
	/*
	funcao alterada em 06-01-2020
	inserido novo parametro onde é enviado o ord_id e o ord_number.
	Na versão anterior era inserido na tabela de order_status na coluna de ord_id o ord_number e agora é salvo o id da tabela order
	e tbm criado a change_date que guardara a data da inserção de novo status
	*/
	public function complete($id='',$id_order=''){

		$id = ($id == '') ? $this->input->post()['ord_number_selecionado']!=""?$this->input->post()['ord_number_selecionado']:"" : $id;

		if(empty($id)){
			redirect(ADMIN."/orders/completed");
		}
		$this->db->set("ord_status","completed");
		$this->db->where("ord_number",$id);
		$this->db->update("order");
		$this->db->insert("order_status",[
			"ord_id"=>$id,
			"ord_cod"=>($id_order == '' ? $this->input->post()['ord_id_selecionado'] : $id_order),
			"change_date"=>time(),
			"usr_id"=>$this->myadmin->get_id(),
			"ors_status"=>"completed",
			"ors_nome_func"=>$this->input->post()['nome_funcionario'] != '' ? $this->input->post()['nome_funcionario'] : "",
			"ors_observation"=>$this->input->post()['observacao'] != '' ? $this->input->post()['observacao'] : ""
		]);
		redirect(ADMIN."/orders/completed");
	}

	
}
/*
    [9] => Array
        (
            [ord_id] => 10
            [sur_id] => 14
            [ord_surfboard_date] => 1551567600
            [mem_id] => 3
            [pkg_id] => 2
            [ord_time] => 1551545479
            [ord_status] => pending
            [sur_title] => Zabo Total Flex
            [sur_location] => 14
            [sur_volume] => Zabo Total Flex
            [sur_size] => 5'4 x 78945 x 554
            [sur_condition] => New product
            [sur_quantity] => 10
            [sur_description] => The Plunder is a fuller-plane shape board designed to bring versatility and fun to small waves without forgetting about that superior performance spark, too. The unique surface area in the outline allows the board to trim with surprising speed in the weakest of conditions, while the soft diamond tail paired with the kick in the rail-line rocker toward the back third, gives the Plunder an ability to respond and turn when a good section lines up.
            [sur_status] => active
            [mem_first] => Developer
            [mem_last] => Testing
            [mem_email] => sarmad711@gmail.com
            [mem_password] => d545481c46ebe5d1e958b7dea80d954044269f7b
            [mem_phone] => 3458341704
            [mem_country] => 168
            [mem_city] => Any
            [mem_zip] => 81400
            [mem_address] => N.S.T , Sargodha
            [mem_pic] => 3a1855f21019cc54b9ca91494752afbb.jpg
            [mem_status] => active
            [mem_verify] => 0
            [pkg_title] => Monthly
            [pkg_type] => SUBSCRIPTIONS
            [pkg_price] => 149
            [pkg_time] => 3
            [pkg_duration] => month
            [pkg_booking_per_day] => 3
            [pkg_cancel_hours] => 0
            [pkg_status] => active
        )
*/