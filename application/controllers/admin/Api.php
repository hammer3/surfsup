<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
	public $data;
	
	public function __construct(){		
		parent::__construct();
		$this->myadmin->is_login();
		if(!$this->myadmin->is_admin()){
			redirect(ADMIN);
		}
	}
	/*Show Login Page*/
	public function index()
	{
		$this->data['row'] = $this->db->get("api")->row();
		if($this->input->post()){
			$this->db->set($this->input->post());
			$this->db->update("api");
			redirect(ADMIN."/api");
		}
		$this->data['page'] = 'api/api';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	
}
