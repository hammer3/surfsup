<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends CI_Controller {
	public $data;
	
	public function __construct(){		
		parent::__construct();
		$this->myadmin->is_login();
		if(!$this->myadmin->is_admin()){
			redirect(ADMIN);
		}
		$this->data['members_active'] = true;
	}
	public function index()
	{
		if($this->input->post()){
			
		}
		$this->data['heading'] = 'Membros Ativos';
		$this->data['row'] = $this->db
						  ->select('*')
						  ->from('members as m')
						  ->join('transaction as t',"t.mem_id=m.mem_id")
						  ->join('packages as p',"p.pkg_id=t.pkg_id")
						  ->where('t.trx_status = "active" ')
						  ->where('p.pkg_status = "active" ')
						  ->group_by('m.mem_id')
						  ->order_by('m.mem_created_time',"DESC")
						  ->get();
                
		$this->data['page'] = 'pages/members';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function inactive()
	{
		if($this->input->post()){
			
		}
		$this->data['heading'] = 'Membros inativos';
				$this->data['row'] = $this->db
								  ->select('*')
								  ->from('members as m1')
								  ->where('mem_id NOT IN (SELECT m.mem_id FROM tbl_members m
									INNER JOIN tbl_transaction t ON t.mem_id = m.mem_id
									INNER JOIN tbl_packages p ON p.pkg_id = t.pkg_id
									WHERE t.trx_status = "active"
									AND p.pkg_status = "active"
									GROUP BY m.mem_id)')
								  ->order_by('m1.mem_created_time',"DESC")
								  ->get();
		
		
		//echo $this->db->last_query();
		
		$this->data['page'] = 'pages/members';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function premium()
	{
		if($this->input->post()){
			
		}
		/*
		
		Select * from tbl_members as m 
		join tbl_transaction as t on t.mem_id=m.mem_id AND t.trx_status='active' 
		join tbl_countries as c on c.c_id=m.mem_country 
		join tbl_packages as p on p.pkg_id=t.pkg_id 
		where m.mem_status = 'active' order by t.trx_id DESC 
		
		*/
		$this->data['heading'] = 'Membros Premium';
		$this->data['row'] = $this->db
								  //->where('mem_status','active')
								  ->select('*, p.pkg_title as package')
								  ->from('members as m')
								  ->join('transaction as t',"t.mem_id=m.mem_id")
								  ->join('countries as c',"c.c_id=m.mem_country")
								  ->join('packages as p',"p.pkg_id=t.pkg_id")
								  ->order_by('t.trx_id',"DESC")
								  ->get();
//		die(print_r($this->data['row']));123456
								  
		$this->data['page'] = 'pages/premium';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	public function viewDetail($id=0){
		
		if($id>0){
			if($var = $data= $this->input->post())
			{	
				$data['mem_status'] = isset($data['mem_status'])?'active':'inactive';
				unset($data['password']);
				unset($data['newpassword']);
				if(!empty($var['password']) AND strlen($var['password'])>5){
					if(md5($var['password']) == md5($var['newpassword'])){
						$data['mem_password'] = $this->myadmin->doEncode($var['password']);
						$this->session->set_flashdata('message_success', "Password Successfully Updated");
					}else{
						$this->session->set_flashdata('message_error', "Password does not Match or less than 6 characters,m If you don't want to update leave it blank.");
					}
				}
				$this->db->where("mem_id",$id);
				$this->db->update("members",$data);
				redirect(ADMIN."/members");
			}
			$this->data['row'] = $this->db->where('mem_id',$id)->order_by('mem_id',"DESC")->get("members")->row();
			$this->data['page'] = 'pages/viewDetail';
			$this->load->view('admin/include/sitemaster',$this->data);
			
		}
		
		
	}
	
	
}
/*

Array
(
    [0] => stdClass Object
        (
            [mem_id] => 2
            [mem_first] => Muhammad
            [mem_last] => Sarmad
            [mem_email] => samgenius@live.com
            [mem_password] => d545481c46ebe5d1e958b7dea80d954044269f7b
            [mem_phone] => 03458341704
            [mem_country] => 168
            [mem_city] => Sargodha
            [mem_zip] => 40100
            [mem_address] => House # 21 Street #38 Block Z new Satlitetown
            [mem_pic] => 8112cd1a3c83efaec9633a1f0af6ddeb.jpg
            [mem_status] => active
            [mem_verify] => 0
            [trx_id] => 7
            [trx_amount] => 318
            [trx_discount] => 0
            [trx_coupon] => na
            [trx_api_id] => TRX789456421D
            [pkg_id] => 1
            [trx_time] => 1551454970
            [trx_cvc] => 7af1304afa686c7a1222b37573dff6d67af40cb7906d135ebadf84626e69f44dfa678ece8fbde15158e01e4a444ec38a58cfcbc37a9f12db8f6fc4f83fe5afd1nAJTkQMx0m1/JIpobKqY7cZgQVFc0ZW5/9CoWHSkhGM=
            [trx_card] => 3593c3204068188ae6a9dbef332b45d44e10e35bbdf943b7e166feb3b80f0e0669740d6e227b2814bed6b6308b121b50aa04cfc1367657372c1ef15af85874d4eH1flky0Ba5kjyYdipSNDYiU8ISf0xgjQILW7SxqfD4=
            [trx_exp_month] => 1a8e733d5ba6ca61f17fc5dc962d979912ca1e9dd5bf5915f19eab28686ec54e3cd68ae59133cf9b242c2aae66bc166b770fe10f1c146eb0e69540946be5b8f6Ld1SZo4gRVjAjoWurSzmWyDwcdMUJbwEyDJX0vgpa6s=
            [trx_exp_year] => 8d5948b5a9fabcf277a7d7238cac957095c2a2d70788c5ca0aa56a5473732dae8725513f9550a67a33802275ef5d2b885eabad5c499654a64839d213a49d3d80uNNwLW7rrfgUx7v6sL0qdX1xRYFHAJ4O0hKUQ3Wftqs=
            [trx_card_name] => 
            [trx_status] => active
            [c_id] => 168
            [c_title] => Pakistan
            [c_code] => PK
            [pkg_title] => Yearly
            [pkg_type] => SUBSCRIPTIONS
            [pkg_price] => 26.5
            [pkg_time] => 1
            [pkg_duration] => year
            [pkg_status] => active
        )

)
*/
