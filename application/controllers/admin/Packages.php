<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends CI_Controller {
	public $data;
	public $where;
	public $type = 'PACKAGES';
	
	public function __construct(){		
		parent::__construct();
		$this->data['packageType'] = array("SUBSCRIPTIONS","PACKAGES");
		$this->data['packageDuration'] = array("credit"=>'credit',"day"=>"Day","month"=>"Month","year"=>"year");
		$this->myadmin->is_login();
		$this->where = array('pkg_type'=>$this->type);
		$this->data['TYPE'] = $this->type;
		if(!$this->myadmin->is_admin()){
			redirect(ADMIN);
		}
		$this->data['packages_active'] = true;
	}
	/*Show Login Page*/
	public function index()
	{
		$this->data['row'] = $this->db->get_where("packages",$this->where);
		$this->data['page'] = 'packages/packages';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	
	public function add($id=''){
		if(!empty($id)){
			$where = ['pkg_id'=>$id];
			$this->data['row'] = $this->db->get_where("packages",$where)->row();
			$this->data['properties'] = $this->db->get_where("packages_properties",$where);
			$this->db->where('pkg_id',$id);			
		}		
		if($this->input->post()){
			$data = $this->input->post();
			unset($data['tpp_property']);
			unset($data['tpp_value']);
			$data['pkg_status'] = isset($data['pkg_status'])?'active':'inactive';
			$data['pkg_type'] = $this->type;
			if(!empty($id)){
				$this->db->set($data);
				$this->db->update("packages");
				$this->properties('update',$id);
				$this->myadmin->success("Packages/Subscription Successfully Updated");
				redirect(ADMIN.'/packages/');
			}
			if($this->db->insert("packages",$data)){
				$insert_id = $this->db->insert_id();
				$this->properties("insert",$insert_id);
				$this->myadmin->success("Packages/Subscription Successfully Saved");
				redirect(ADMIN.'/packages/');
			}else{
				$this->myadmin->success("Error While Saving");
			}
		}
		
		$this->data['page'] = 'packages/add-packages';
		$this->load->view('admin/include/sitemaster',$this->data);
	}
	private function properties($do='insert',$id=''){
		$data = $this->input->post();
		if(empty($id)){
			return false;
		}
		if($do=='update'){
			$this->db->where('pkg_id',$id);
			$this->db->delete("packages_properties");
		}
		for($i=0;$i<count($data['tpp_property']);$i++){
			if(!empty($data['tpp_property'][$i]) AND !empty($data['tpp_value'][$i])){
				$this->db->set('tpp_property',$data['tpp_property'][$i]);
				$this->db->set('tpp_value',$data['tpp_value'][$i]);
				$this->db->set('pkg_id',$id);
				$this->db->insert("packages_properties");
			}

		}
		//die($this->db->last_query());
		return true;
		
		
	}
	public function delete($id)
	{
		if(!empty($id)){
			$this->db->where('pkg_id',$id);
			if($this->db->delete("packages")){
				$this->myadmin->success("Successfully Deleted");
				redirect(ADMIN.'/packages/');
			}
		}
		$this->myadmin->error("Error While Deleting");
		redirect(ADMIN.'/packages/');
	}
	
	//public function add_
	
}
