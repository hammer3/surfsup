<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbstractOcurrences extends CI_Controller {
  public $data;
	public $planId;
	public $memberId;
	public $api;
	public $plan_id;
	public function __construct(){
		parent::__construct();
		
    $this->api = $this->db->get("api")->row();
	}

  public function renewPlans(){
    echo '33'; 
    $today = date('Y-m-d'); 
    $tomorrow = date('Y-m-d', strtotime("+1 day")); 
    
    $this->db->select('*');
    $this->db->from('tbl_transaction');
    $this->db->where('trx_status', 'active');
    $this->db->where('trx_end <=',$tomorrow);
    $this->db->where('trx_end >=',$today);

    $needRenew = $this->db->get();  
    $curl = curl_init();
    $this->body = [
			"api_key"=>$this->api
		];
    foreach($needRenew->result() as $row){
      $url = 'https://api.pagar.me/1/subscriptions/'. $row->trx_refer_id;

      curl_setopt_array($curl, array(
	        CURLOPT_RETURNTRANSFER => true,
	        CURLOPT_URL => $url,
	        CURLOPT_CUSTOMREQUEST => 'POST',
	        CURLOPT_HTTPHEADER => ['content-type: application/json'],
			CURLOPT_POSTFIELDS => json_encode($this->body),
			CURLOPT_ENCODING => 'utf-8'
	    ));
	
	    $response = curl_exec($curl);
	    $err = curl_error($curl);

      $newValues = json_decode($response);

      if($newValues != '')  {
       
        $dataBegin = date($newValues["message"]->current_period_start  , strtotime("Y-m-d"));
        $dataEnd   = date($newValues["message"]->current_period_end  , strtotime("Y-m-d"));
        
        $this->db->where('trx_refer_id', $row->trx_refer_id);
        $this->db->set('trx_begin', $dataBegin);
        $this->db->set('trx_end', $dataEnd);
        $this->db->update('tbl_transaction');
      }
      else{
        $this->db->where('trx_refer_id', $row->trx_refer_id);
        $this->db->set('trx_status', 'inactive');
        $this->db->set('trx_is_renewed', 0);
        $this->db->set('trx_renewed_error', 'Erro durante a renovação !');
        $this->db->update('tbl_transaction');  
      }
    }

    curl_close($curl);     

  }
}