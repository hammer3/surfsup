<?php

/*This is a Custom MVC Developed By Muhammad Samad*/
date_default_timezone_set("UTC");
$getURL = key($_GET);

if(!empty($getURL)){
	$getURL = rtrim($getURL,'/');
	$getURL = ltrim($getURL,'/');
	$getURL = explode("/",$getURL);
}
else{
	$getURL = ['home'];
}

require_once("config/config.php");
require_once('config/base_path.php');

//die(API_URL);
require_once("controller/Master.php");
$API = new Request(); //Api Request 
try{
	require_once('controller/'.$getURL[0].'_c.php');
	$fun = "{$getURL[0]}_c";
	$con = new $fun();
	unset($getURL[0]);
	if(isset($getURL[1])){
		$function = $getURL[1];
		unset($getURL[1]);
		$argunments=array_values($getURL);
		if(!empty($argunments)){
			call_user_func_array(array($con,$function),$argunments);
		}else{
			call_user_func(array($con,$function));
		}
		
	}else{

		$con->index();
	}
}catch(Exception $e){
	die('404 Page Not Found');
}





?>