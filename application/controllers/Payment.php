<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {
	public $data;
	public $planId;
	public $memberId;
	public $api;
	public $plan_id;
	public function __construct(){
		parent::__construct();
		//var_dump($this->website_m->checkEmail());exit;
		if($this->website_m->checkEmail()){
			$this->session->set_flashdata('message_success','Por favor, atualize seu e-mail primeiro');
			redirect("account");
		}
		//check if user loggedin
		$this->website_m->is_login();
		$this->memberId = $this->website_m->get_member_id();
//		var_dump($this->session->userdata("planid"));die();
		if($this->session->has_userdata("planid")){
			$this->planId = $this->session->userdata("planid")->pkg_id;
			$this->pagar_id = $this->session->userdata("planid")->pagar_id;
		}else{
			redirect("packages");
		}
		//echo 'Na linha 26 de Payment() Get api: <p> ';
		//var_dump($this->db->get("api")->row());
		
		//exit;
		$this->api = $this->db->get("api")->row();
	}
	public function index($msg='')
	{
		//var_dump($this->api->api_private);die('index payment');
		$this->data['api_public'] = $this->api->api_private; //public and privated mistakenly swaped while development
		$this->data['page'] = 'payment';
		$this->data['package'] = $this->website_m->get_packages(['pkg_id'=>$this->planId,'pkg_status'=>'active'])->row();

		$this->data['price'] = $this->calculatePrice();
		//var_dump($this->data);
		//die('fim debug');
		if($msg=='success'){
			if($this->session->flashdata("redirectHere")){
				$this->output->set_header('refresh:3; url='.base_url($this->session->flashdata("redirectHere")));
			}else{
				$this->output->set_header('refresh:3; url='.base_url("browse-surfboards"));
			}
			unset($_SESSION['planid']);
			//die("payment done");
		}
		$this->load->view('website/credit-card',$this->data);
	}
	public function checkCoupon($couponz){
			$allPackage= $this->db->get_where("packages",['pkg_status'=>'active'])->result_array();
			$getColumn = array_column($allPackage,'pkg_title','pkg_id');
			$couponType = ([
				'all'=>'All',
				'package'=>'All Packages',
				'subscription'=>'All Subscriptions'
			]+$getColumn);
			//print_r($couponType);
			//exit;	
			if(empty($couponz)){
				return 0;		
			}
			
			$this->db->where("cop_code",$couponz);
			$this->db->where("cop_status",'active');
			$coupon = $this->db->get("coupon")->row();
			
			$session = $this->session->userdata("planid");
			$show=false;

			if(!empty($couponz)){
				if(md5(strtoupper($coupon->cop_code))==md5(strtoupper($couponz))){
					if(in_array($couponType[$coupon->pkg_id],$couponType)){
						if($session->pkg_id==$coupon->pkg_id){
							$show = true;
						}
						else{
							if('all'==$coupon->pkg_id){
								$show = true;
							}else if('package'==$coupon->pkg_id AND $session->pkg_type=='PACKAGES') {
								$show = true;
							}
							else if('subscription'==$coupon->pkg_id AND $session->pkg_type=='SUBSCRIPTIONS') {
								$show = true;
							}
						}
						
					}
				}
			}
			
		if($show){
			return $coupon->cop_discount;
		}
		return 0;
		
		
	}
	public function purchase(){
		/**aqui carrega o paconte existente */
		if($this->planId = $this->session->userdata("planid")->pkg_id){
			//die($this->planId->pkg_id);
		}else{
			$this->session->set_flashdata('select_plan','Por favor, compre seu plano primeiro.');
			redirect("plans");
		}
		if($this->input->post()){
		//verificando se a transation esta ativada	
		if($data = $this->do_transaction()){
				$user_id = $this->website_m->get_member_id();
				$member = $this->db->get_where("members",['mem_id'=>$user_id])->row();
				$country_code  = $this->website_m->get_countries($member->mem_country)->row()->c_code;
			
				if(empty($country_code)){
					$country_code = 'GB';
				}
				//	trx_id	trx_api_id	mem_id	pkg_id	trx_time	trx_cvc	trx_card	trx_exp_month	trx_exp_year
				$var = $this->input->post();//dados do post
				$dataPlan = $this->session->userdata['planid'];//dados do plano
				$discount = ($this->calculatePrice()/100)*$this->checkCoupon($var['coupon']);

				//verificando cupon
				$coupon = !empty($this->input->post()['coupon'])?$this->input->post()['coupon']:'na';
				
				//echo '<pre>';				var_dump($dataPlan);				die('fim debug');
				
				$namePackg = $dataPlan->pkg_title;
				//SUBSCRIPTIONS or PACKAGES
				$planType = $this->session->userdata['planid']->pkg_type;
				$pkg_duration = $dataPlan->pkg_duration;//year, month,day
				$pkg_time = $dataPlan->pkg_time;//quantidade de dias
				$month = 1;
				$isPack = false;
				$isSubscription = false;
				if($planType=='SUBSCRIPTIONS'&&$pkg_duration=='month'){
					$days = 30;
					$isSubscription = true;
				
				}else if($planType=='SUBSCRIPTIONS'&&$pkg_duration=='year'){
					$month = 12;
					$days = 365;
					$isSubscription = true;
				}else{
					$isPack = true;
					$days = $pkg_time;
				}
				
				if(($this->calculatePrice()-$discount)==0){
					goto skipPayment;
				}

				$payment = new pagar($this->api->api_public); //público e privado trocados por engano enquanto o desenvolvimento

				if(empty($this->pagar_id) OR $discount>0){//com desc
					//tem desconto, então pesquiso pacote com este valor, se não tem
					(int)$amount=(($this->calculatePrice()-$discount) * 100); //format em centavos
					$dataFind = [
						'amount'=>$amount,
						'days'=>$days,
						'month'=>$month
					];
					
					$findPlanExist= $payment->findPlan($dataFind);
					if($findPlanExist==NULL){//se não encontrou
						// precisa criar outro plano
						$namePackg = $dataPlan->pkg_title;
	
						$data = [
							'amount'=>$amount,
							'days'=>$days,
							'month'=>$month,
							'name'=>$namePackg. ($coupon!="na"?'_'.$coupon:""),///aqui deve ser o mesmo nome do plano a ser descontado
						];
						//echo '$dataNewPlan:<pre>';var_dump($data);die();

						//criando um novo plano com o valor calculado, retorna o id
						$idplan = $payment->create_plan($data);
			
					}else{
						//id retornado do plano encontrado com desconto
						$idplan = $findPlanExist;
					}
				//valor descontado
					$n_price = (int)$amount;
					
					//criando uma nova transaction
					if($isPack==true){

						$charge = $payment->chargeNow([
							"trx_amount"=>$n_price,
							'fullName'=>$var['fullName'],
							"cardNo"=>$var['cardNo'],
							"expMonth"=>$var['expMonth'],
							"expYear"=>$var['expYear'],
							"cardCVC"=>$var['cardCVC'],
							"token"=>$var['token'],
							"mem_email"=>$member->mem_email,
							"mem_passport"=>$member->mem_passport,
							"mem_phone"=>$member->mem_phone,
							"mem_address"=>$member->mem_address,
							"mem_country"=>$country_code,
							"mem_zip"=>$member->mem_zip,
							"mem_city"=>$member->mem_city
						]);
					}else{
						$charge = $payment->create_subscription([
							"trx_amount"=>$n_price,
							'fullName'=>$var['fullName'],
							"cardNo"=>$var['cardNo'],
							"expMonth"=>$var['expMonth'],
							"expYear"=>$var['expYear'],
							"cardCVC"=>$var['cardCVC'],
							"token"=>$var['token'],
							"mem_email"=>$member->mem_email,
							"mem_passport"=>$member->mem_passport,
							"mem_phone"=>$member->mem_phone,
							"mem_address"=>$member->mem_address,
							"mem_country"=>$country_code,
							"mem_zip"=>$member->mem_zip,
							"mem_city"=>$member->mem_city,
							"plan_id"=>$idplan				
						]);
					}
				}else{//sem desc ou sem vinculo com plano
					$n_price = (int)$this->calculatePrice();
					$idplan = $this->pagar_id;

					$charge = $payment->create_subscription([
						"trx_amount"=>$n_price,
						'fullName'=>$var['fullName'],
						"cardNo"=>$var['cardNo'],
						"expMonth"=>$var['expMonth'],
						"expYear"=>$var['expYear'],
						"cardCVC"=>$var['cardCVC'],
						"token"=>$var['token'],
						"mem_email"=>$member->mem_email,
						"mem_passport"=>$member->mem_passport,
						"mem_phone"=>$member->mem_phone,
						"mem_address"=>$member->mem_address,
						"mem_country"=>$country_code,
						"mem_zip"=>$member->mem_zip,
						"mem_city"=>$member->mem_city,
						"plan_id"=>$idplan				
					]);
		
				}
        
      if($charge["error"]=="no"){
				skipPayment: //if payment == 0 
        
        $dateB = substr($charge["message"]->current_period_start ,0 ,10);    
        $dateE = substr($charge["message"]->current_period_end ,0 ,10);
        
       
				$submitData = [
						'trx_amount'=>$this->calculatePrice()-$discount,
						'trx_discount'=>$discount,
						'trx_coupon'=>$coupon,
						'trx_api_id'=>isset($charge["message"]->tid)?$charge["message"]->tid:'',
						'mem_id'=>$this->memberId,
						'pkg_id'=>$this->planId,
						'trx_time'=>time(),
						'trx_card_name'=>$var['fullName'],
						'trx_response'=>isset($charge["message"])?json_encode($charge["message"]):'',
            'trx_refer_id' => $charge["message"]->id, 
            'trx_begin' => $dateB,
            'trx_end' => $dateE,
            'trx_is_renewed' => 1
          ];
					
          ///////////////////////////////////////////
					if($this->db->insert("transaction",$submitData)){
						$this->session->set_flashdata('message_success','Transação realizada com sucesso');
						$url = "home/congratulations";
						$this->session->set_flashdata("redirectHere",$url);
						redirect("payment/index/success");
					}
				}else{
					$this->session->set_flashdata('message_error',$charge["message"]);
					redirect("payment");
				}
			}else{
				$this->session->set_flashdata('message_error','A transação está desativada pelo administrador do site');
				redirect("payment");
			}
			
		}else{
			redirect("payment");
		}
	}

  private function do_transaction(){
		return true;
	}
	private function calculatePrice(){
		$package = $this->website_m->get_packages(['pkg_id'=>$this->planId],['pkg_status'=>'active'])->row();
		$price = $package->pkg_price;  
		$time_credit = $package->pkg_time; //credits in package case
		$duration = $package->pkg_duration; //1month , 1 year etc
		
		if($package->pkg_type="SUBSCRIPTIONS"){
			if($duration=='year'){
				$price = $price*12;
			}
		}
		return $price;
	}
	
	
	

}