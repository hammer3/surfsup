
<footer>
    <div class="contain text-center">
        <div class="footerLogo"><a href="<?=base_url();?>"><img src="<?=base_url(CLIENT_ASSETS);?>images/logo.svg" alt=""></a></div>
        <ul class="lst flex">
            <li><a href="<?=base_url();?>">Início</a></li>
            <li><a href="<?=base_url();?>about">Quem somos</a></li>
            <li><a href="<?=base_url();?>how-it-works">Como funciona</a></li>
            <li><a href="<?=base_url();?>help">Perguntas frequêntes</a></li>
            <li><a href="javascript:void(0)" class="popBtn" data-popup="newsletter">Newsletter</a></li>
            <li><a href="<?=base_url();?>contact">Fale conosco</a></li>
        </ul>
        <div class="copyright relative">
            <ul class="list relative">
                <li>direito autoral © <?=date("Y");?> <a href="<?=base_url();?>">Surf's up Club</a> CNPJ.: 30.145.560/0001-03</li>
                <li><a href="<?=base_url();?>privacy-policy">Política de Privacidade</a></li>
                <li><a href="<?=base_url();?>toc">termos e Condições</a></li>
            </ul>
        </div>
        <p class="small"><?=$this->website_m->website()->footer_text;?></p>
        <ul class="social flex">
            <li><span class="semi">Siga-nos</span></li>
            <li><a href="<?=$this->website_m->website()->site_facebook;?>" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
            <li><a href="<?=$this->website_m->website()->site_twitter;?>" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
            <li><a href="<?=$this->website_m->website()->site_instagram;?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
            <li><a href="<?=$this->website_m->website()->site_youtube;?>" target="_blank"><i class="fa fa-youtube-square"></i></a></li>
        </ul>
    </div>
    <div class="popup small-popup" data-popup="newsletter">
        <div class="tableDv">
            <div class="tableCell">
                <div class="contain">
                    <div class="_inner">
                        <div class="crosBtn"></div>
                        <h3>Boletim de Notícias</h3>
                        <p class="text-center">Promoções exclusivas, brindes, dicas de profissionais, comunidade, coxinhas e muito mais!</p>
                        <form action="" method="post">
                            <div class="txtGrp">
                                <input type="text" name="email" id="email" class="txtBox" placeholder="Email Address">
                            </div>
                            <div class="bTn text-center">
                                <button type="submit" class="webBtn colorBtn lgBtn">Se inscrever</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer -->


<!-- Main Js -->
<script type="text/javascript" src="<?=base_url(CLIENT_ASSETS);?>js/main.js?v=<?=time();?>"></script>

