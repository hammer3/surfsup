<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMNR93G" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="ease">
    <div class="contain">
        <div class="logo ease">
            <a href="index.php"><img src="<?=base_url(CLIENT_ASSETS);?>images/logo.svg" alt=""></a>
        </div>
        <div class="toggle"><span></span></div>
        <nav class="semi ease">
            <ul id="nav">
                <li class="<?php if($page=="home"){echo 'active';} ?>">
                    <a href="<?=base_url();?>"><?=$this->website_m->CMS('buttons')->my_home_button?></a>
                </li>
                <li class="<?php if($page=="surfs-up" || $page=="about" || $page=="how-it-works" || $page=="why-join" ){echo 'active';} ?> drop">
                    <a href="javascript:void(0)"><?=$this->website_m->CMS('buttons')->my_Surfs_up_button?></a>
                    <ul class="sub">
                        <li class="<?php if($page=="about"){echo 'active';} ?>">
                            <a href="<?=base_url();?>about"><?=$this->website_m->CMS('buttons')->my_Surfs_up_1stbutton?></a>
                        </li>
                        <li class="<?php if($page=="how-it-works"){echo 'active';} ?>">
                            <a href="<?=base_url();?>how-it-works"><?=$this->website_m->CMS('buttons')->my_Surfs_up_2ndbutton?></a>
                        </li>
                        <li class="<?php if($page=="why-join"){echo 'active';} ?>">
                            <a href="<?=base_url();?>why-join"><?=$this->website_m->CMS('buttons')->my_Surfs_up_3rddbutton?></a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if($page=="plans" || $page=="browse-surfboards" || $page=="contact"){echo 'active';} ?>">
                    <a href="<?=base_url();?>plans"><?=$this->website_m->CMS('buttons')->my_plan_button?></a>
                </li>
                <li class="<?php if($page=="browse-surfboards"){echo 'active';} ?>">
                    <a href="<?=base_url();?>browse-surfboards"><?=$this->website_m->CMS('buttons')->my_browse_surfboard_button?></a>
                </li>
                <li class="<?php if($page=="contact"){echo 'active';} ?>">
                    <a href="<?=base_url();?>contact"><?=$this->website_m->CMS('buttons')->my_contact_button?></a>
                </li>
            </ul>
            <ul id="logged">
                <li class="<?php if($page=="signin"){echo 'active';} ?> btnLi">
                    <a href="<?=base_url();?>signin">Entre</a>
                </li>
                <li class="<?php if($page=="signup"){echo 'active';} ?>">
                    <a href="<?=base_url();?>signup">Cadastre-se</a>
                </li>
            </ul>
        </nav>
    </div>
</header>
<!-- header -->


<div class="upperlay"></div>
<!-- <div id="pageloader">
    <span class="loader"></span>
</div> -->


<!-- Header Fix Js -->
<script type="text/javascript">
    $(document).ready(function() {
        var offSet = $('body').offset().top;
        var offSet = offSet + 5;
        $(window).scroll(function() {
            var scrollPos = $(window).scrollTop();
            if (scrollPos >= offSet) {
                $('header').addClass('fix');
            } else {
                $('header').removeClass('fix');
            }
        });
    });
</script>
