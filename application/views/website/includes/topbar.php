<?php
	$header = $this->website_m->future_resrvation_header();
?>
<div id="topnav">
    <div class="contain">
        <div class="inside">
            <div id="owl-navigation" class="owl-carousel owl-theme">
                <a href="<?=base_url();?>account" class="<?php if($page=="account"){echo 'active';} ?>">Minha conta</a>
                <a href="<?=base_url();?>reservation" class="<?php if($page=="reservation"){echo 'active';} ?>">Minha reserva <?=(isset($header)?'<span class="miniLbl green">'.$header.'</span>':'');?></a>
                <a href="<?=base_url();?>packages" class="<?php if($page=="packages"){echo 'active';} ?>">Meus pacotes</a>
                <a href="<?=base_url();?>signout" class="<?php if($page=="signin"){echo 'active';} ?>">Sair</a>
            </div>
			<? if(!empty($this->website_m->getPlanHeader())): ?>
            <h6><a href="<?=base_url();?>packages"><?=$this->website_m->getPlanHeader();?></a></h6>
			<? endif; ?>
        </div>
    </div>
</div>
