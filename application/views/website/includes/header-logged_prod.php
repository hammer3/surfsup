<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MMNR93G" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<?php
$this->website_m->future_resrvation_header();
?>
<header class="ease fix">
    <div class="contain">
        <div class="logo ease">
            <a href="<?=base_url();?>"><img src="<?=base_url(CLIENT_ASSETS);?>images/logo.svg" alt=""></a>
        </div>
        <div class="toggle"><span></span></div>
        <div class="proIco dropDown">
            <div class="inside dropBtn">
				<a href="<?=base_url();?>reservation"><?=(isset($header)?"<strong>Reservas ({$header})</strong>":"<strong>Reservas (0)</strong>");?></a>
                <div class="ico">
                    <img src="<?=base_url(UPLOAD_AVATAR).($this->website_m->get_member_image()?$this->website_m->get_member_image():'avatar.png');?>" alt="">
                </div>
            </div>
            <ul class="proDrop dropCnt">
                <li><a href="<?=base_url();?>account"><i class="fi-webpage"></i>Minha Conta</a></li>
                <li><a href="<?=base_url();?>packages"><i class="fi-list"></i>Pacotes</a></li>
                <li><a href="<?=base_url();?>help"><i class="fi-question-circle"></i>Ajuda</a></li>
                <li><a href="<?=base_url();?>signout"><i class="fi-power-switch"></i>Sair</a></li>
            </ul>
        </div>
        <nav class="semi ease">
            <ul id="nav">
                <li class="<?php if($page=="home"){echo 'active';} ?>">
                    <a href="<?=base_url();?>"><?=$this->website_m->CMS('buttons')->my_home_button?></a>
                </li>
                <li class="<?php if($page=="surfs-up" || $page=="about" || $page=="how-it-works" || $page=="why-join" ){echo 'active';} ?> drop">
                    <a href="javascript:void(0)"><?=$this->website_m->CMS('buttons')->my_Surfs_up_button?></a>
                    <ul class="sub">
                        <li class="<?php if($page=="about"){echo 'active';} ?>">
                            <a href="<?=base_url();?>about"><?=$this->website_m->CMS('buttons')->my_Surfs_up_1stbutton?></a>
                        </li>
                        <li class="<?php if($page=="how-it-works"){echo 'active';} ?>">
                            <a href="<?=base_url();?>how-it-works"><?=$this->website_m->CMS('buttons')->my_Surfs_up_2ndbutton?></a>
                        </li>
                        <li class="<?php if($page=="why-join"){echo 'active';} ?>">
                            <a href="<?=base_url();?>why-join"><?=$this->website_m->CMS('buttons')->my_Surfs_up_3rddbutton?></a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if($page=="plans" || $page=="browse-surfboards" || $page=="contact"){echo 'active';} ?>">
                    <a href="<?=base_url();?>plans"><?=$this->website_m->CMS('buttons')->my_plan_button?></a>
                </li>
                <li class="<?php if($page=="browse-surfboards"){echo 'active';} ?>">
                    <a href="<?=base_url();?>browse-surfboards"><?=$this->website_m->CMS('buttons')->my_browse_surfboard_button?></a>
                </li>
                <li class="<?php if($page=="contact"){echo 'active';} ?>">
                    <a href="<?=base_url();?>contact"><?=$this->website_m->CMS('buttons')->my_contact_button?></a>
                </li>
            </ul>
        </nav>
    </div>
</header>
<!-- header -->


<div class="upperlay"></div>
<!-- <div id="pageloader">
    <span class="loader"></span>
</div> -->
