<!doctype html>
<html>
<head>
<title>404 Não encontrado – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<main>


<section id="not404">
    <div class="flexDv">
		<div class="contain">
		    <div class="inside flex">
		        <div class="ico"><span class="bold">404</span></div>
		        <h1 class="secHeading">404 Página não encontrada</h1>
		        <p class="italic">Vamos fingir.....!! Que você não viu isso. <span><a href="<?=base_url();?>">Clique aqui</a> voltar a página inicial.</span></p>
		    </div>
		</div>
    </div>
</section>
<!-- not404 -->


</main>
</body>
</html>