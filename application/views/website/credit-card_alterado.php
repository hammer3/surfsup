<!doctype html>
<html>
<head>
<title>Credit card – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php require_once('includes/header-logged.php'); ?>
<main>


<section id="sBanner" style="background-image: url('<?=base_url(CLIENT_ASSETS);?>images/surf-1479730_1920.jpg');">
    <div class="contain">
        <div class="content">
            <h1>Cartão de crédito</h1>
            <ul>
                <li><a href="<?=base_url();?>">Casa</a></li>
                <li>Pagamento</li>
            </ul>
        </div>
    </div>
</section>
<!-- sBanner -->
<section id="credit">
    <div class="contain">
        <div class="inner">
            <div class="planBlk">
               <? 
			   
			    $pre = '';
				if( strtolower($package->pkg_duration)=='year'){
					$pre = '12x';
					
				}
				if(strtolower($package->pkg_duration)=='month' || strtolower($package->pkg_duration)=='year'){
					$package->pkg_duration = 'mês';
					
				} else if(strtolower($package->pkg_duration)=='day'){
  				$package->pkg_duration = '&nbsp;dia';
  			}
			   
			   
			   if($package->pkg_type=="SUBSCRIPTIONS"): 
			   
				
			   
			   
			   ?>
				
				
					<!-- <h3>Prime</h3> -->
					<div class="icon"><i class="fi-diamond"></i></div>
					<div class="price"><?=$package->pkg_title;?></div>
					<small><?=$pre;?>R$<?=$package->pkg_price;?> /por <?=$package->pkg_duration;?></small>
					<ul class="list">
					<? foreach(@$this->website_m->get_properties_by_id($package->pkg_id)->result() as $property): ?>
						<li><?=$property->tpp_property;?> <em> <?=$property->tpp_value;?></em></li>
					<? endforeach; ?>
					</ul>
			
			   <? else: ?>
			
					<!-- <h3>Flex</h3> -->
					<div class="icon"><i class="fi-glass"></i></div>
					<div class="price"><?=$package->pkg_title;?></div>
					<small><?=$pre;?>R$<?=$package->pkg_price;?> /por <?=$package->pkg_time.$package->pkg_duration.($package->pkg_time>1?'s':'');?></small>
					<ul class="list">
					<? foreach($this->website_m->get_properties_by_id($package->pkg_id)->result() as $property): ?>
						<li><?=$property->tpp_property;?> <em> <?=$property->tpp_value;?></em></li>
					<? endforeach; ?>
					</ul>
				
			   <? endif; ?>
            </div>
            <div class="content">
                <form action="<?=base_url();?>payment/purchase" method="post" id="payment_form">
                    <div class="couponCode blk">
                        <div class="blockLst">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>Nome do pacote</th>
                                        <td class="price" width="100">R$ <span id="oldPrice"><?=$price?></span></td>
                                    </tr>                                   
									<tr>
                                        <th>cupom de desconto</th>
                                        <td class="price">
                                            <div class="txtGrp couponBlk">
                                                <input type="text" name="coupon" id="coupon" class="txtBox" placeholder="Insira cupom">
                                            </div>
                                            <span id="coupon_price">R$ 0</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>total</th>
                                        <td class="price">R$ <span id="newPrice"><?=$price?></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="creditBlk">
                        <div data-payment="credit-card">
						<!--Show Message Success/error-->
						<? if($this->session->flashdata('message_success')): ?>
							<div class="alert alert-success">
								<?=$this->session->flashdata('message_success');?>
							</div>

						<? endif; ?>
						<? if($this->session->flashdata('message_error')): ?>
							<div class="alert alert-danger">
								 <?=$this->session->flashdata('message_error');?>
							</div>

						<? endif; ?>
						<!--End Show Message Success/error-->
                            <div class="topHead">
                                <h2 class="color">Pague com cartão de crédito</h2>
                                <ul class="cardLst flex">
                                    <li><img src="<?=base_url(CLIENT_ASSETS);?>images/payment-visa.svg" alt=""></li>
                                    <li><img src="<?=base_url(CLIENT_ASSETS);?>images/payment-master.svg" alt=""></li>
                                    <li><img src="<?=base_url(CLIENT_ASSETS);?>images/payment-american-express.svg" alt=""></li>
                                    <li><img src="<?=base_url(CLIENT_ASSETS);?>images/payment-discover.svg" alt=""></li>
                                    <li><img src="<?=base_url(CLIENT_ASSETS);?>images/payment-jcb.svg" alt=""></li>
                                    <li><img src="<?=base_url(CLIENT_ASSETS);?>images/payment-diners-club.svg" alt=""></li>
                                </ul>
                            </div>
                            <div class="row formRow">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xx-12 txtGrp">
                                    <input type="text" pattern="[0-9]{16}" required name="cardNo"  id="card_number" class="txtBox" placeholder="Número do cartão"><div class="input-validation"></div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xx-12 txtGrp">
                                    <input type="text"  required name="fullName" id="card_holder_name" class="txtBox" placeholder="Nome no cartão"><div class="input-validation"></div>
                                </div>
                                <? 
                                    $meses = array(
                                        'January'=>'Janeiro',
                                        'February'=>'Fevereiro',
                                        'March'=>'Março',
                                        'April'=>'Abril',
                                        'May'=>'Maio',
                                        'June'=>'Junho',
                                        'July'=>'Julho',
                                        'August'=>'Agosto',
                                        'September'=>'Setembro',
                                        'October'=>'Outubro',
                                        'November'=>'Novembro',
                                        'December'=>'Dezembro'
                                );
                                ?>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                                    <select id="card_expiration_month" required name="expMonth"  class="txtBox selectpicker" data-live-search="true">
                                        <option value="">Mês</option>
									<? for($i=1;$i<=12;$i++): ?>
									   <option value="<?=date("m",strtotime(date("Y-{$i}-1")));?>"><?=$meses[date("F",strtotime(date("Y-{$i}-1")))];?></option>
                                    <? endfor; ?>
                                    </select>
                                </div>
								<div class="input-validation"></div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                                    <select id="card_expiration_year" required name="expYear" class="txtBox selectpicker" data-live-search="true">
                                        <option value="">Ano</option>
                                        <? for($i=date("y");$i<=date("y")+12;$i++): ?>
										   <option value="<?=$i;?>"><?=$i;?></option>
										<? endfor; ?>
                                    </select>
                                </div>
								<div class="input-validation"></div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xx-12 txtGrp">
                                    <input type="text" required name="cardCVC" id="card_cvv" class="txtBox" placeholder="CVC?"><div class="input-validation"></div>
                                </div>
								<input type="hidden" id="token" value="" name="token">
                            </div>
                        </div>
                        <div class="btmHead">
                            <div class="bTn">
                                <button type="submit" class="webBtn colorBtn lgBtn">Efetuar Pagamento</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- credit -->
<script>
$(document).ready(function() { 
	$("#coupon").blur(function(){
		$.post("<?=base_url("home/checkCoupon")?>",$(this).serialize(),function(data){
			reply = JSON.parse(data);
			//console.log(data);
			
			if(reply.valid=="no"){
				$("#newPrice").html(<?=$price?>);				
				$("#coupon_price").html("$ 0");
				alert("Cupom inválido ou expirado");				
				
			}else if(reply.valid=="yes"){
				discount = reply.discount;
				oldprice = <?=$price?>;
				newPrice = eval((oldprice/100)*discount);
                //limitando para 2 casas decimais o resultado do calculo
                var numformat = eval(oldprice-newPrice).toFixed(2);

				$("#newPrice").html(numformat);
                $("#newPrice").val(numformat);
                
				$("#coupon_price").html("$ "+newPrice);
				alert(discount+"% Cupom aplicado.");
			}
		});
	});

    var form = $("#payment_form")

    form.submit(function(event) {
        //event.preventDefault();
        var card = {} 
        card.card_holder_name = $("#payment_form #card_holder_name").val()
        card.card_expiration_date = $("#payment_form #card_expiration_month").val() + '/' + $("#payment_form #card_expiration_year").val()
        card.card_number = $("#payment_form #card_number").val()
        card.card_cvv = $("#payment_form #card_cvv").val()

        // pega os erros de validação nos campos do form e a bandeira do cartão
        var cardValidations = pagarme.validate({card: card})
		var myerror=true;
        //Então você pode verificar se algum campo não é válido
        if(!cardValidations.card.card_number){
			$("#payment_form #card_number").addClass("myerror");	
			myerror = false;
		}else{
			$("#payment_form #card_number").removeClass("myerror");
		}
		if(!cardValidations.card.card_expiration_month){
			$("#payment_form #card_expiration_month").addClass("myerror");	
			myerror = false;
		}else{
			$("#payment_form #card_expiration_month").removeClass("myerror");
		}
		if(!cardValidations.card.card_expiration_year){
			$("#payment_form #card_expiration_year").addClass("myerror");	
			myerror = false;
		}else{
			$("#payment_form #card_expiration_year").removeClass("myerror");
		}
		if(!cardValidations.card.card_holder_name){
			$("#payment_form #card_holder_name").addClass("myerror");	
			myerror = false;
		}else{
			$("#payment_form #card_holder_name").removeClass("myerror");
		}
		if(myerror == false){
			return false;
		}

        pagarme.client.connect({ encryption_key: '<?=$api_public;?>' })
          .then(client => client.security.encrypt(card))
          .then(card_hash => console.log(card_hash));
		  
        form.submit();
		return true;
    })
});

</script>
<style>
.myerror{
	border: 1px solid #e81010;
}
/* When the pattern is matched */
input[type="text"]:valid {
    color: green;
}
.input-validation{
	position: absolute;
    bottom: 0 !important;
	right:0;
}
input[type="text"]:valid ~ .input-validation::before {
    content: "\2713";
    color: green;
}

/* Unmatched */
input[type="text"]:invalid {
    color: red;
}

</style>
</main>
<?php require_once('includes/footer.php');?>
</body>
</html>