<!doctype html>
<html>
<head>
<title>Assinar - Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php //require_once('includes/header.php'); ?>
<main>


<section id="logOn">
    <div class="flexDv">
		
        <div class="contain">
            <div class="logBlk">
                <div class="loginLogo"><a href="<?=base_url();?>"><img src="<?=base_url(CLIENT_ASSETS);?>images/logo.svg" alt=""></a></div>
				<? if($showSteps): ?>
				<div class="txtGrp text-center steps">
					<p><span>Passo 1 de 3:</span> Por favor, preencha as informações do seu perfil para criar sua conta no Surf's Up</p>
				</div>
				<? endif; ?>
				
				<form action="" method="post" autocomplete="false">
                    <h2>Inscreva-se com o Surf's up</h2>
					
					<!--Show Message Success/error-->
				<? if($this->session->flashdata('message_success')): ?>
					<div class="alert alert-success">
						<?=$this->session->flashdata('message_success');?>
					</div>

				<? endif; ?>
				<? if($this->session->flashdata('message_error')): ?>
					<div class="alert alert-danger">
						 <?=$this->session->flashdata('message_error');?>
					</div>

				<? endif; ?>
				<!--End Show Message Success/error-->
				<? if(validation_errors()): ?>
					<div class="alert alert-danger text-left">
						 <?=validation_errors();?>
					</div>

				<? endif; ?>
					
                    <div class="txtGrp">
                        <input type="text" name="mem_name"  value="<?=set_value('mem_name');?>" id="" class="txtBox " autocomplete="false" placeholder="Nome completo">
                    </div>
                    <div class="txtGrp">
                        <input type="email" name="mem_email" id=""  value="<?=set_value('mem_email');?>"  class="txtBox" placeholder="Endereço de e-mail">
                    </div>
					<div class="txtGrp">
                        <input type="text" name="mem_phone" id=""  value="<?=set_value('mem_phone');?>"  class="txtBox" placeholder="número de telefone">
                    </div>
                    <div class="txtGrp">
                        <input type="password" name="mem_password" id="" class="txtBox" placeholder="Senha">
                    </div>
                    <div class="txtGrp">
                        <input type="password" name="mem_repassword" id="" class="txtBox" placeholder="Confirme a Senha">
                    </div>
                    <div class="rememberMe">
                        <div class="lblBtn">
                            <input required type="checkbox" name="confirm" checked id="confirm">
                            <label for="confirm">Ao inscrever-se, eu concordo com o Surf's up
                                <a href="<?=base_url();?>toc">Termos e Condições</a>
                                <a href="<?=base_url();?>privacy-policy">Política de Privacidade.</a>
                            </label>
                        </div>
                    </div>
                    <div class="bTn text-center">
                        <button type="submit" class="webBtn colorBtn">Criar conta</button>
						<br><br>
						<h2>Conectar com Social</h2>
                        <a href="<?=base_url("signin/facebook_login");?>" class="webBtn colorBtn" style="background-color:#29487d" ><i class="fa fa-facebook"></i> Registrar com o Facebook</a>
                        <a href="<?=base_url("signin/google_login");?>" class="webBtn colorBtn" style="background-color:#b23121" ><i class="fa fa-google" ></i> Registrar com o Google</a>
                    </div>
                </form>
                <div class="haveAccount text-center">
                    <span>Já tem uma conta?</span>
                    <a href="signin">Entrar</a>
                </div>
                <ul class="miniNav semi">
                    <li><a href="<?=base_url();?>privacy-policy">Política de Privacidade</a></li>
                    <li><a href="<?=base_url();?>contact">Contato</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--  logOn -->


</main>
<?php //require_once('includes/footer.php');?>
</body>
</html>