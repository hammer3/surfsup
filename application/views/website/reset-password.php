<!doctype html>
<html>
<head>
<title>Redefinir Senha - Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php //require_once('includes/header.php'); ?>
<main>


<section id="logOn">
    <div class="flexDv">
        <div class="contain">
            <div class="logBlk">
                <div class="loginLogo"><a href="<?=base_url();?>"><img src="<?=base_url(CLIENT_ASSETS);?>images/logo.svg" alt=""></a></div>
                <form action="" method="post">
                    <h2>Redefinir Senha</h2>
                    <p>Digite uma nova senha para sua conta.</p>
					<?=isset($message)?'<p style="color:red">'.$message.'</p>':'';?>
                    <div class="txtGrp">
                        <input type="password" name="newpassword" id="" class="txtBox" placeholder="Nova senha">
                    </div>
                    <div class="txtGrp">
                        <input type="password" name="renewpassword" id="" class="txtBox" placeholder="Confirme a Senha">
                    </div>
                    <div class="bTn text-center">
                        <button type="submit" class="webBtn colorBtn">Enviar</button>
                    </div>
                </form>
                <ul class="miniNav semi">
                    <li><a href="<?=base_url();?>privacy-policy">Política de Privacidade</a></li>
                    <li><a href="<?=base_url();?>contact">Contato</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--  logOn -->


</main>
<?php //require_once('includes/footer.php');?>
</body>
</html>