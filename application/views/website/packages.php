<!doctype html>
<html>
<head>
<title>Meus pacotes – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}
$btnCaseSelected = '<a href="'.base_url('reservation/cancelPlanNow').'" class="confirmCancel"><span>plano escolhido <i class="fi-check"></i></span></a>';
$btnCaseSelected2 = '<a href="javascript:void(0);" ><span>plano escolhido <i class="fi-check"></i></span></a>';
?>
<main>


<section id="dash">
    <?php require_once('includes/topbar.php'); ?>
    <div class="contain">
        <div class="mainBlk" id="packages">
            <div class="blk">
                <div class="_header">
                    <h3>Meus pacotes</h3>
                </div>
                <div class="content text-center">
				<? if($this->session->flashdata('select_plan')): ?>
					<div class="alert alert-danger">
						 <?=$this->session->flashdata('select_plan');?>
					</div>

				<? endif; ?>
				<? if($this->session->flashdata('success_order')): ?>
					<div class="alert alert-success">
						<?=$this->session->flashdata('success_order');?>
					</div>

				<? endif; ?>
				<? if($this->session->flashdata('error_order')): ?>
					<div class="alert alert-danger">
						 <?=$this->session->flashdata('error_order');?>
					</div>

				<? endif; ?>
                    <h2>Assinaturas</h2>
                    <ul class="lst flex mysub">
					
                     <? foreach($subscription->result() as $perSub):
							$pre = '';
							if( strtolower($perSub->pkg_duration)=='year'){
								$pre = '12x';
								
							}
							if(strtolower($perSub->pkg_duration)=='month' || strtolower($perSub->pkg_duration)=='year'){
								$perSub->pkg_duration = 'mês';
							}
							
					 ?>
						<li>
							<div class="planBlk <?=@$selected->pkg_id==$perSub->pkg_id?'selected':'';?>">
								<!-- <h3>Prime</h3> -->
								<div class="icon"><i class="fi-diamond"></i></div>
								<div class="price"><?=$perSub->pkg_title;?></div>
								<small><?=$pre;?>R$ <?=$perSub->pkg_price;?> /por <?=$perSub->pkg_duration;?></small>
								<ul class="list">
								<? foreach(@$this->website_m->get_properties_by_id($perSub->pkg_id)->result() as $property): ?>
									<li><?=$property->tpp_property;?> <em> <?=$property->tpp_value;?></em></li>
								<? endforeach; ?>
								</ul>
								<div class="bTn"><?=@$selected->pkg_id==$perSub->pkg_id?$btnCaseSelected:$this->website_m->showButton($perSub->pkg_id);?></div>
							</div>
						</li>
					<? endforeach; ?>
                        					
                    </ul>
                    <hr>
                    <h2>Pacotes</h2>
                    <ul class="lst lst2 flex">
                     
                    <? foreach($packages->result() as $perPkg): 
						$pre = '';
						if( strtolower($perPkg->pkg_duration)=='year'){
							$pre = '12x';
							
						}					
						if(strtolower($perPkg->pkg_duration)=='month' || strtolower($perPkg->pkg_duration)=='year'){
							$perPkg->pkg_duration = 'mês';
						}
						
			
					
					?>
					<li>
						<div class="planBlk <?=@$selected->pkg_id==$perPkg->pkg_id?'selected':'';?>">
							<!-- <h3>Flex</h3> -->
							<div class="icon"><i class="fi-glass"></i></div>
							<div class="price"><?=$perPkg->pkg_title;?></div>
							<small><?=$pre;?>R$<?=$perPkg->pkg_price;?> /por <?=$perPkg->pkg_time.$perPkg->pkg_duration.($perPkg->pkg_time>1?'s':'');?></small>
							<ul class="list">
							<? foreach($this->website_m->get_properties_by_id($perPkg->pkg_id)->result() as $property): ?>
								<li><?=$property->tpp_property;?> <em> <?=$property->tpp_value;?></em></li>
							<? endforeach; ?>
							</ul>
							<div class="bTn"><?=$this->website_m->showButton($perPkg->pkg_id);?></div>
						</div>
					</li>
				<? endforeach; ?>
				  
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- dash -->
<style>
#packages .mysub .selected:hover{
	border: 2px solid #f30a0a !important;
}	
#packages .mysub .selected:hover >  .bTn a {
    color: #ef0732 !important;
	
}	
#packages .mysub .selected:hover .bTn a span {
	display: none;
}

#packages .mysub .selected:hover .bTn a:after{
content: "Cancelar agora \f00d" !important;
font-family: FontAwesome;
}

</style>

</main>
<?php require_once('includes/footer.php');?>
</body>
</html>
