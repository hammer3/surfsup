<!doctype html>
<html>
    <head>
        <title>Browse Surfboards – Surf's up Club</title>
        <?php require_once('includes/site-master.php'); ?>
    </head>
    <body id="home-page">
        <?php
        if ($this->website_m->is_login("header"))
        {
            require_once('includes/header-logged.php');
        }
        else
        {
            require_once('includes/header.php');
        }
        ?>
        <main>
        <!-- <section id="sBanner" style="background-image: url('images/surf-1479730_1920.jpg');">
            <div class="contain">
                <div class="content">
                    <h1>Browse Surfboards</h1>
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li>Browse Surfboards</li>
                    </ul>
                </div>
            </div>
        </section> -->
            <!-- sBanner -->


            <section id="browse">
                <div class="srchBlk">
                    <div class="contain">
                        <form action="<?= base_url('browse-surfboards'); ?>" method="get" class="srchBar">
                            <ul class="srchLst relative">
                                <li>
                                    <input type="text" name="query" id="" value="<?= @$query ?>" class="txtBox" placeholder="Pesquisar">
                                </li>
                                <input type="hidden" value="location" >

                                <li><button type="submit"   class="webBtn"><i class="fi-arrow-right"></i></button></li>
                            </ul>
                        </form>
                    </div>
                </div>
                <div class="contain">
                    <? if ($showSteps): ?>
                        <div class="txtGrp text-center steps">
                            <p><span>Passo 3 de 3:</span> Parabéns, você é o mais novo membro do Surf's Up Club. Reserve sua prancha filtrando por localização e/ou filtros avançados</p>
                        </div>
<? endif; ?>
                    <div class="topHead">			
                        <h2 class="bold">Olá <?= $this->website_m->getFirstName(); ?>, com qual prancha você gostaria de surfar hoje?</h2>
                        <div style="width:180px;padding:0 10px 0 10px; ">
                            <form action="" method="get" name="lc_submit">
                                <select name="location"  id="" class="txtBox selectpicker" onchange="lc_submit.submit();" >
                                    <option value="">Localização</option>
                                        <? foreach ($this->myadmin->get_location_category()->result() as $category): ?>
                                        <optgroup label="<?= $category->loc_title ?>">
                                            <? foreach ($this->myadmin->get_location(['loc_id' => $category->loc_id, 'lcc_status' => 'active'])->result() as $locationz): ?>
                                                <option value="<?= $locationz->lcc_id; ?>"><?= $locationz->lcc_title ?></option>
    <? endforeach; ?>								
<? endforeach; ?>
                                </select>
                            </form>
                        </div>
                        <button type="button" class="webBtn smBtn colorBtn filterBtn"><i class="fi-slider"></i> <?= $buttons->filter_surf_button; ?></button>
                    </div>
                    <div id="filters">
                        <form action="<?= base_url('browse-surfboards'); ?>" method="get" >
                            <ul class="filterLst flex">
                                <li>
                                    <h4>Período</h4>
                                    <input type="text" name="sur_dates" id="" class="txtBox datepicker" placeholder="Dates">
                                </li>
                                <li>
                                    <h4>Tipos</h4>
                                    <select name="sur_type[]" id="" class="txtBox selectpicker" multiple="">
                                            <? foreach ($type_S->result() as $attribute): ?>
                                                <option value="<?= $attribute->type_id; ?>"><?= $attribute->sur_types; ?></option>
                                            <? endforeach; ?>
                                    </select>
                                </li>
                                <li>
                                    <h4>Marcas</h4>
                                    <select name="sur_brand[]" id="" class="txtBox selectpicker" multiple="">

                                    <? foreach ($brand_S->result() as $attribute): ?>
                                            <option value="<?= $attribute->brand_id; ?>"><?= $attribute->sur_brands; ?></option>
                                    <? endforeach; ?>
                                    </select>
                                </li>
                                <li>
                                    <h4>Comprimento</h4>
                                    <input type="text" name="sur_length" id="length" class="txtBox">
                                </li>
                                <li>
                                    <h4>Volume</h4>
                                    <input type="text" name="sur_volume" id="volume" class="txtBox">
                                </li>


                            </ul>
                            <div class="bTn text-right"><button type="submit" value="1" name="apply_filter" class="webBtn colorBtn">Aplique</button></div>
                        </form>
                    </div>
                    <? if ($this->session->flashdata("error_order")): ?>
                        <div class="alert alert-danger">
                            <strong>Aviso </strong><?= $this->session->flashdata("error_order") ?>
                        </div>
<? endif; ?>
<? if ($this->website_m->checkAccounMissing()): ?>
                        <div class="alert alert-info" style="display:none;">
                            <strong>Aviso.</strong> Por favor preencha o perfil para processar com a reserva. <a href="<?= base_url("account") ?>" target="_blank">Clique aqui</a>
                        </div>

<? endif; ?>
                    <ul class="lst flex text-center">
<? foreach ($surfboards->result() as $board): ?>
                            <li>
                                <div class="iTm">
                                    <div class="image">
                                        <a href="<?= base_url(); ?>detalhe-produto/<?= $board->sur_id ?>/<?= my_url($board->sur_title); ?>"><img src="<?= base_url() . UPLOAD_PATH . @$this->website_m->get_surfboard_image(['sur_id' => $board->sur_id], 1)->row()->sui_images; ?>" alt=""></a>
                                    </div>
                                    <div class="txt">
                                        <h4><a href="<?= base_url(); ?>detalhe-produto/<?= $board->sur_id ?>/<?= my_url($board->sur_title); ?>"><?= $board->sur_title; ?></a></h4>
                                        <div class="specs"><?= $board->sur_size; ?></div>
                                        <div class="ltr"><?= $board->sur_volume; ?> L</div>
                                        <div class="ltr"><a href="<?= base_url(); ?>detalhe-produto/<?= $board->sur_id ?>/<?= my_url($board->sur_title); ?>" class="">Ver detalhes</a></div>
                                        <div class="bTn">
                                            <?php if($board->sur_inmanutencao==1){ ?>
                                                <a href="javascript:void(0);" class="webBtn btn-default ReservePopup">Reservado</a>
                                                    <?php }else{ ?>
                                                <a href="javascript:void(0);" myhref="<?= base_url('home/product_detail/' . $board->sur_id . '/' . my_url($board->sur_title) . '/json'); ?>" class="webBtn colorBtn ReservePopup"><?= $buttons->filter_surf_button_signout; ?></a>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </li>
<? endforeach; ?>   

                    </ul>


                    <ul class="pagination">
<?= $this->pagination->create_links(); ?>
                    </ul>

                </div>
            </section>
            <!-- browse class="active"-->
            <!------template------------->
            <div class="popup large-popup" data-popup="surfBoardReservtion" id="surfBoardReservtion">
                <div class="tableDv">
                    <div class="tableCell">
                        <div class="contain">
                            <div class="_inner">
                                <form action="<?= base_url("reservation/reserveNow"); ?>" method="post">
                                    <div class="crosBtn"></div>
                                    <h3>Detalhes da prancha</h3>                        
                                    <div class="blockLst" id="proDetail" style="padding-top:10px;margin-top:10px;">

                                        <div class="contain">
                                            <label>Selecione datas para reservar</label>
                                            <input type="text" readonly name="selectedDate" id="" class="txtBox datepicker2" value="" placeholder="Select Date">
                                            <br>
                                            <hr>
                                            <div class="flexRow flex">


                                                <div class="col col1">
                                                    <div class="miniSlider">
                                                        <img src=""  alt="" class="surfboard_image">
                                                    </div>												

                                                </div>
                                                <div class="col col2">
                                                    <div class="content ckEditor">
                                                        <h2 class="sur_title"></h2>

                                                        <hr>
                                                        <div class="location semi">Localização: <em class="color regular surfboard_location"></em></div>

                                                        <hr>
                                                        <!--<h5>Condição: <em class="color surfboard_condition"></em></h5>-->

                                                        <ul>
                                                            <li><strong>Volume:</strong> <span class="sur_volume"></span> L </li>
                                                            <li><strong>Tamanho:</strong> <span class="sur_size"></span></li>					
                                                            <li><strong>Marca:</strong> <span class="sur_brand"></span></li>
                                                            <!---

<li><strong>Type:</strong> </li>						
<li><strong>Brand:</strong> </li>
<li><strong>Volume:</strong></li>
<li><strong>Tamanho:</strong> </li>

------>	
                                                        </ul>

                                                        <hr>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>

                                    </div>


                                    <div class="bTn text-center">
                                        <button type="submit" class="webBtn lgBtn colorBtn">Reserve agora</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---------------->
            <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
            <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
            <!-- Ion Slider -->
            <link type="text/css" rel="stylesheet" href="<?= base_url(CLIENT_ASSETS); ?>css/ion.slider.min.css">
            <script type="text/javascript" src="<?= base_url(CLIENT_ASSETS); ?>js/ion.slider.min.js"></script>
            <script type="text/javascript" src="<?= base_url(CLIENT_ASSETS); ?>js/ion.slider.skins.js"></script>
            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $('#length').ionRangeSlider({
                                                        skin: 'square',
                                                        from: 5,
                                                        to: 10,
                                                        min: 5,
                                                        max: 10,
                                                        // values: [
                                                        //     "4'8", "9'9"
                                                        // ],
                                                        // prettify_separator: ",",
                                                        // min: 10,
                                                        // max: 10000,
                                                        type: 'double',
                                                        // postfix: '"',
                                                        step: 1,
                                                        grid: true
                                                    });
                                                    $('#volume').ionRangeSlider({
                                                        skin: 'square',
                                                        from: 20,
                                                        to: 80,
                                                        min: 20,
                                                        max: 80,
                                                        type: 'double',
                                                        prettify: function (num) {
                                                            return num + ' L';
                                                        },
                                                        grid: true
                                                    });
                                                });
            </script>
            <script type="text/javascript">
                $(function () {
                    $(document).on('click', '#browse .filterBtn', function () {
                        $('#browse #filters').slideToggle();
                    });
                });
            </script>
        </main>
<?php require_once('includes/footer.php'); ?>
    </body>
</html>