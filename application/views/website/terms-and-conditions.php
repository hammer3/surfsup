<!doctype html>
<html>
<head>
<title>Terms & Conditions – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}	
?>
<main>


<section id="sBanner" style="background-image: url('<?=base_url(CLIENT_ASSETS);?>images/surf-1479730_1920.jpg');">
    <div class="contain">
        <div class="content">
            <h1>Termos e Condições</h1>
            <ul>
                <li><a href="index.php">Casa</a></li>
                <li>Termos e Condições</li>
            </ul>
        </div>
    </div>
</section>
<!-- sBanner -->


<section id="terms">
    <div class="contain">
        <div class="blk ckEditor">
            <div class="_header">
                <h3><?=$website_tos->tos_heading;?></h3>
            </div>
            <?=$website_tos->tos_detail;?>
        </div>
    </div>
</section>
<!-- terms -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>