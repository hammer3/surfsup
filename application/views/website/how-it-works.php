<!doctype html>
<html>
<head>
<title>Como isto funciona – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}	
?>
<main>


<!-- <section id="sBanner" style="background-image: url('<?=base_url(CLIENT_ASSETS);?>images/surf-1479730_1920.jpg');">
    <div class="contain">
        <div class="content">
            <h1><?=@$website_how->top_main_heading;?></h1>
            <p class="pre" style="margin-top:10px;"><?=@$website_how->top_main_sub_heading;?></p>
        </div>
    </div>
</section> -->
<!-- sBanner -->


<section id="works" class="worksPg">
    <div class="block working">
        <div class="contain">
            <div class="content">
				<!-- <h5 class="text-center"><?=@$website_how->howItMainHeading1;?></h5> -->
                <h5 class="text-center">UMA ÚNICA MENSALIDADE, o cliente tem direito a:</h5>
                <ul class="listing flex">
                    <li>
                        <div class="inner">
                            <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->picture1);?>" alt=""></div>
                            <h4><?=@$website_how->text1;?></h4>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->picture2);?>" alt=""></div>
                            <h4><?=@$website_how->text2;?></h4>
                        </div>
                    </li>
                    <li>
                        <div class="inner">
                            <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->picture3);?>" alt=""></div>
                            <h4><?=@$website_how->text3;?></h4>
                        </div>
                    </li>
                </ul>
                <div class="bTn text-center"><a href="<?=base_url();?>help" class="webBtn lgBtn colorBtn"><?=$buttons->how_button_top?></a></div>
            </div>
        </div>
    </div>
    <div class="block whyUs">
        <div class="contain text-center">
            <h1 class="secHeading"><?=@$website_how->howItMainHeading2;?></h1>
            <ul class="lst flex">
                <li>
                    <div class="inner">
                        <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->why_picture1);?>" alt=""></div>
                        <div class="cntnt">
                            <h3><?=@$website_how->why_heading1;?></h3>
                            <p><?=@$website_how->why_text1;?></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->why_picture2);?>" alt=""></div>
                        <div class="cntnt">
                            <h3><?=@$website_how->why_heading2;?></h3>
                            <p><?=@$website_how->why_text2;?></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->why_picture3);?>" alt=""></div>
                        <div class="cntnt">
                            <h3><?=@$website_how->why_heading3;?></h3>
                            <p><?=@$website_how->why_text3;?></p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="inner">
                        <div class="icon"><img src="<?=base_url(UPLOAD_PATH."website/".$website_how->why_picture4);?>" alt=""></div>
                        <div class="cntnt">
                             <h3><?=@$website_how->why_heading4;?></h3>
                            <p><?=@$website_how->why_text4;?></p>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="bTn text-center"><a href="<?=base_url();?>plans" class="webBtn lgBtn colorBtn"><?=$buttons->how_button_bottom?></a></div>
        </div>
    </div>
    <div class="block contactUs">
        <div class="contain">
            <div class="content text-center">
                <h1 class="secHeading"><?=@$website_how->contact_heading;?></h1>
                <p><?=@$website_how->contact_description;?></p>
            </div>
        </div>
    </div>
</section>
<!-- works -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>