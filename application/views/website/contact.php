<!doctype html>
<html>
<head>
<title>Contatar-nos – Surf's up Club</title>
<?php require_once('includes/site-master.php'); ?>
</head>
<body id="home-page">
<?php 
if($this->website_m->is_login("header")){
	require_once('includes/header-logged.php'); 
}else{
	require_once('includes/header.php');
}	
?>

<main>

<!-- <section id="sBanner" style="background-image: url('<?=base_url(CLIENT_ASSETS);?>images/surf-1479730_1920.jpg');">
    <div class="contain">
        <div class="content">
            <h1>Contate-Nos</h1>
            <ul>
                <li><a href="<?=base_url();?>">Casa</a></li>
                <li>Contate-Nos</li>
            </ul>
        </div>
    </div>
</section> -->
<!-- sBanner -->


<section id="contact">
    <div class="contain">
        <div class="flexRow flex">
            <div class="col col1">
                <div class="content">
                    <h3><?=$website_contact->contact_heading?></h3>
                    <?=$website_contact->contact_detail?>
                    <h4>Questões ou Comentários</h4>
                    <ul class="lst inFo">
                        <li><i class="fi-phone"></i><a href="tel:<?=$this->website_m->website()->site_phone;?>"><?=$this->website_m->website()->site_phone;?></a></li>
                        <li><i class="fi-envelope"></i><a href="mailto:<?=$this->website_m->website()->site_email;?>"><?=$this->website_m->website()->site_email;?></a></li>
                        <li><i class="fi-map-marker"></i><?=$this->website_m->website()->site_address;?></li><!--<span>70 east sunrise highway, <br> V35 - 80 Dto 1070-020 Lisboa, <br> New York 11581.</span>--->
                    </ul>
                </div>
            </div>
            <div class="col col2">
                <h3>Deixe-nos uma mensagem</h3>
				<? if($this->session->flashdata('message_success')): ?>
					<div class="alert alert-success">
						<?=$this->session->flashdata('message_success');?>
					</div>

				<? endif; ?>
				<? if($this->session->flashdata('message_error')): ?>
					<div class="alert alert-danger">
						 <?=$this->session->flashdata('message_error');?>
					</div>

				<? endif; ?>
				<!--End Show Message Success/error-->
				<? if(validation_errors()): ?>
					<div class="alert alert-danger">
						 <?=validation_errors();?>
					</div>

				<? endif; ?>
                <form action="" method="post">
                    <div class="row formRow">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                            <input type="text" name="cnt_name" id="" class="txtBox" placeholder="Nome">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                            <input type="text" name="cnt_phone" id="" class="txtBox" placeholder="Telefone">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                            <input type="text" name="cnt_subject" id="" class="txtBox" placeholder="Assunto">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xx-6 txtGrp">
                            <input type="email" name="cnt_email" id="" class="txtBox" placeholder="E-mail">
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xx-12 txtGrp">
                            <textarea name="cnt_message" id="" class="txtBox" placeholder="Mensagem"></textarea>
                        </div>
                    </div>
                    <div class="bTn"><button type="submit" class="webBtn colorBtn lgBtn"><?=$buttons->contactUsPageButton;?></button></div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- contact -->


</main>
<?php require_once('includes/footer.php');?>
</body>
</html>