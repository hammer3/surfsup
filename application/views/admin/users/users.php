
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Tables</a>
		</li>
		<li class="active">
			<strong>Data Tables</strong>
		</li>
	</ol>
	
	<h3>Adicionar / Remover Local</h3>
		<div class="text-right">
			<a href="<?=base_url(ADMIN.'/users/add');?>" class="btn btn-info btn-lg btn-icon icon-left "> <i class="entypo-plus"></i> Adicionar usuários</a>
		</div>
		<br><br>
		<script type="text/javascript">
		jQuery( window ).load( function() {
			var $table4 = jQuery( "#table-4" );
			$table4.DataTable({
				
				dom: 'Bfrtip',
				buttons: [
					 
					 {
						  extend: 'excel',
						  text: 'Export to Excel',
						  exportOptions: {
								columns: 'th:not(:last-child)'
							}
					 }
				]
			});
		});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>SN</th>
					<th>O email</th>
					<th>Localização</th>
					<th>Status</th>
					<th>Ações</th>
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					
					<td><?=$sn++;?></td>
					<td><?=$r->usr_email;?></td>
					<td><?=@$r->lcc_title;?></td>
					<td><?=ucfirst($r->usr_status);?></td>
					<td>
						<a href="<?=base_url(ADMIN."/users/add/{$r->usr_id}");?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Editar
						</a>
						
						<a href="<?=base_url(ADMIN."/users/delete/{$r->usr_id}");?>" class="btn btn-danger btn-sm btn-icon icon-left delete_confirm">
							<i class="entypo-cancel"></i>
							Excluir
						</a>
						
					</td>
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		
