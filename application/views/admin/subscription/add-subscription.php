
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Subscription</a>
		</li>
		<li class="active">
			<strong>Add/Edit Subscription</strong>
		</li>
	</ol>
	
	<h3>Add/Edit Subscription</h3>
	<form role="form" action="" method="post" class="form-horizontal form-groups-bordered">
			
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Title</label>
				
				<div class="col-sm-5">
					<input type="text" name="pkg_title" value="<?=@$row->pkg_title;?>" class="form-control" id="field-1" placeholder="Title">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Price Per Month $</label>
				
				<div class="col-sm-5">
				
					<div class="input-spinner">
						<button type="button" class="btn btn-primary btn-sm">-</button>
						<input type="text" name="pkg_price" class="form-control size-1 input-sm" value="<?=@$row->pkg_price;?>" value="1" />
						<button type="button" class="btn btn-primary btn-sm">+</button>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Duration/Time</label>
				
				<div class="col-sm-5">
				
					
					<div class="input-group">

						<input type="text" name="pkg_time" class="form-control size-1 input-sm " value="<?=@$row->pkg_time;?>" value="1" />

					
						<select name="pkg_duration" required class="form-control">
							<option value="">--Select--</option>
						
						<? foreach($packageDuration as $val=>$duration): ?>
							<option <?=$val==@$row->pkg_duration?'selected':'';?> value="<?=$val;?>"><?=$duration;?></option>
						<? endforeach; ?>
						</select>
					</div>
					
				</div>
				
			</div>
			
			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" name="pkg_status" <?=(@$row->pkg_status=='active')?'checked':'';?> />
					</div>
				</div>
			</div>
			<div class="col-sm-12">
			<center>
				<div style="width:80%">
					<table class="table table-bordered property_table ">
						<thead>
							<tr>
								<td colspan="4"><h2>Add Remove Packages Properties</h2></td>
							</tr>
							<tr>
								<th>Property</th>
								<th>Value</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
				<?php
				if(isset($properties)):
					foreach($properties->result() as $property): ?>
							<tr>
								<td><input type="text" value="<?=$property->tpp_property;?>" name="tpp_property[]" class="form-control size-4 input-sm" placeholder="Property" /></td>
								<td><input type="text" value="<?=$property->tpp_value;?>" name="tpp_value[]" class="form-control size-4 input-sm" placeholder="Value" /></td>
								<td>
									<a href="" class="btn btn-danger btn-sm btn-icon icon-left remove_property">
										<i class="entypo-trash"></i>
										Delete
									</a>
									
								</td>
							</tr>
				<?php
					endforeach;
				endif;
				?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4" align="right">
									<a href="#" class="btn btn-info btn-sm btn-icon icon-left add_property"> <i class="entypo-plus"></i> Add New Property</a>
								</td>
							</tr>
						</tfoot>

					</table>
				</div>
			</div>
			
			<div class="col-sm-12">
			<center>
				<div style="width:40%">
					<table class="table table-bordered  ">
						<thead>
							<tr>
								<td colspan="3"><h2>Configuration For Above Subscription</h2></td>
							</tr>
							<tr>
								<th>Config</th>
								<th>Value</th>
								
							</tr>
						</thead>
						<tbody>
				
							<tr>
								<td>
									Number of booking Per Day<br>
									<small>0 mean unlimited</small>
								</td>
								<td>
									<div class="form-group">
															
										<div class="col-sm-5">
											
												<input type="number" required="true" min="0" value="<?=@$row->pkg_booking_per_day;?>" name="pkg_booking_per_day" value="1" required value="1" />Day/Days
										
										</div>
									</div>
								</td>
								
							</tr>
							<tr>
								<td>
									Cancel Before(Time in Hours)<br>
									<small>eg: 72 Hours only, 0 men cannot Cancel</small>
								</td>
								<td>
									<div class="form-group">
															
										<div class="col-sm-5">
											
												<input type="number" min="0"  required="true" value="<?=@$row->pkg_cancel_hours;?>" name="pkg_cancel_hours" required value="12" />Hours
										
										</div>
									</div>
								</td>
								
							</tr>
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3" align="right">
									
								</td>
							</tr>
						</tfoot>

					</table>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-8">
					<input type="submit"  class="btn btn-success btn-lg" value="Save">
				</div>
			</div>
	</form>
<script>
$(function(){
	$('.add_property').click(function(e){
		e.preventDefault();
		$('.property_table tbody').append('<tr> <td><input type="text" name="tpp_property[]" class="form-control size-4 input-sm" placeholder="Property" /></td> <td><input type="text" name="tpp_value[]" class="form-control size-4 input-sm" placeholder="Value" /></td>  <td> <a href="" class="btn btn-danger btn-sm btn-icon icon-left remove_property"> <i class="entypo-trash"></i> Delete </a> </td> </tr>').removeProperty();
	});
	$.fn.removeProperty = function(){
		$(".remove_property").each(function(){
			$(this).click(function(e){
				e.preventDefault();
				$(this).parent().parent().remove();
			});
		});
		
	}
	$(".remove_property").each(function(){
		$(this).click(function(e){
			e.preventDefault();
			$(this).parent().parent().remove();
		});
	});
	$(".remove_img").each(function(){
		$(this).click(function(e){
			e.preventDefault();
			$(this).parent().find("input[type=hidden]").val("deleted");
			$(this).parent().hide(500);
		});
	});
});
</script>