<div class="row">
		
			<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
							<li>
							<? if(!$this->myadmin->is_admin()): ?>
								Your Location: <?=$this->myadmin->get_location(['lcc_id'=>$this->myadmin->is_admin('location')])->row()->lcc_title;?>
							<? endif; ?>
						
					</li>
				
				
				
		
			</div>
		
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">
		
					<!-- Language Selector -->
		
					<li class="sep"></li>
		
					

					<li>
						<a href="<?=base_url();?>"  >
							<i class="entypo-globe"></i>
							Visit Website
		
	
						</a>
					</li>
		
					<li class="sep"></li>
		
					<li>
						<a href="<?=base_url(ADMIN.'/signout');?>">
							Log Out <i class="entypo-logout right"></i>
						</a>
					</li>
				</ul>
		
			</div>
		
		</div>
		
		<hr />
		