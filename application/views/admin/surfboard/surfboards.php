
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Tables</a>
		</li>
		<li class="active">
			<strong>Data Tables</strong>
		</li>
	</ol>
	
	<h3>Add/Remove Surfboards</h3>
		<form action="" method="post">
		<div class="text-right">
			<button type="submit" class="btn btn-info btn-lg" >Update Order</button>
			<a href="<?=base_url(ADMIN.'/surfboards/add');?>" class="btn btn-info btn-lg btn-icon icon-left "> <i class="entypo-plus"></i> Add Surfboard</a>
		</div>
		<br><br>
		<script type="text/javascript">
			$(function() {
			var $table4 = $( "#table-4" );
			$table4.DataTable({
				dom: 'Bfrtip',
				buttons: [
					'excel'
				],
				rowReorder: {
					selector: '.myreorder'
				},
				reorder:true,
				
				columnDefs: [
					{ orderable: true, className: 'reorder', targets: 0 },
					{ orderable: false, targets: '_all' }
				],
				"bPaginate": false,
				"bLengthChange": false,
				"bFilter": true,
				"bInfo": false,
				"bAutoWidth": false
			});
			 
		});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					<th>Ordem</th>
					<th>ID da Prancha</th>
					<th>Brand</th>
					<th>Título</th>
					<th>Type</th>
					<th>Localização</th>
					<th>Size</th>
					<th>Volume</th>					
					<th>Condição</th>
					<th>Status</th>
					<th>Ações</th>
				</tr>
			</thead>			 
			<tbody>
				<!--sur_title	sur_location	sur_volume	sur_size	sur_condition	sur_quantity	sur_status-->
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					<!-- <td class="myreorder">
						<?=$r->sur_id;?>
					</td> -->
					<td class="myreorder">
						<?=$sn++;?>
						
					</td>
					<td><?=$r->sur_id;?></td>
					<td><?=$r->sur_brands;?></td>
					<td>
						<?=$r->sur_title;?>
						<input type="hidden" name="order[]" value="<?=$r->sur_id;?>">
					</td>
					<td><?=$r->sur_types;?></td>
					<td><?=$r->lcc_title;?></td>
					<td><?=$r->sur_size;?></td>
					<td><?=$r->sur_volume;?></td>
					<td><?=$r->sur_condition;?></td>					
					<td><?=ucfirst($r->sur_status);?></td>
					<td>
						<a href="<?=base_url(ADMIN."/surfboards/add/{$r->sur_id}");?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Editar
						</a>				
						<a href="<?=base_url(ADMIN."/surfboards/emManutencao/{$r->sur_id}/{$r->sur_inmanutencao}");?>" class="btn 
							<?php echo $r->sur_inmanutencao==1?'btn-info':'btn-default'; ?> btn-sm btn-icon icon-left">
							<i class="entypo-tools"></i>
							Em Manutenção
						</a>
						<a href="<?=base_url(ADMIN."/surfboards/delete/{$r->sur_id}");?>" class="btn btn-danger btn-sm btn-icon icon-left delete_confirm">
							<i class="entypo-cancel"></i>
							Deletar
						</a>
						
					</td>
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		
		</form>
