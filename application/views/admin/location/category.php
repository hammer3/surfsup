
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Region</a>
		</li>
		<li class="active">
			<strong>Region</strong>
		</li>
	</ol>
	
	<h3>Add/Remove Location Region</h3>
		<p style="color:red">Note:- Delete may effect Surfboards data and Previous History even Payment, make sure no other data depend on it. Status Inactive preffer instead of delete</p>
		<div class="text-right">
			<a href="<?=base_url(ADMIN.'/location/addCategory');?>" class="btn btn-info btn-lg btn-icon icon-left "> <i class="entypo-plus"></i> Add Location Category</a>
		</div>
		<br><br>
		<script type="text/javascript">
		jQuery( window ).load( function() {
			var $table4 = jQuery( "#table-4" );
			$table4.DataTable();
		});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					<th>SN</th>
					<th>Category</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					
					<td><?=$sn++;?></td>
					<td><?=$r->loc_title;?></td>
					<td><?=ucfirst($r->loc_status);?></td>
					<td>
						<a href="<?=base_url(ADMIN."/location/addCategory/{$r->loc_id}");?>" class="btn btn-default btn-sm btn-icon icon-left">
							<i class="entypo-pencil"></i>
							Edit
						</a>
						
						<a href="<?=base_url(ADMIN."/location/deleteCategory/{$r->loc_id}");?>" class="btn btn-danger btn-sm btn-icon icon-left delete_confirm">
							<i class="entypo-cancel"></i>
							Delete
						</a>
						
					</td>
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		
