	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Order</a>
		</li>
		<li class="active">
			<strong><?=$heading;?></strong>
		</li>
	</ol>
	
	<h3><?=$heading;?></h3>
		
		<br><br>
		<script type="text/javascript">
		$( function() {
			var $table4 = jQuery( "#table-4" );
			$table4.DataTable({	
				dom: 'Bfrtip',
				"scrollX":true,
				buttons: [
					 {
						  extend: 'excel',
						  text: 'Exportar para Excel',
						  exportOptions: {
								columns: 'th:not(:last-child)'
							}
					 }
				],
				rowReorder: {
					selector: '.myreorder'
				},
				reorder:true,

				columnDefs: 
				[
					{ orderable: true, className: 'reorder', targets: 0},
					{ orderable: true, className: 'reorder', targets: 1 },
					{ orderable: true, className: 'reorder', targets: 2,  'visible':true },
					{ orderable: true, className: 'reorder', targets: 3 },
					{ orderable: true, className: 'reorder', targets: 4,  'visible':true },
					{ orderable: true, className: 'reorder', targets: 5,  'visible':true },
					{ orderable: true, className: 'reorder', targets: 6 , 'visible':true },
					{ orderable: true, className: 'reorder', targets: 7,  'visible':true },
					{ orderable: true, className: 'reorder', targets: 8 , 'visible':true },
					{ orderable: true, className: 'reorder', targets: 9 , 'visible':true },
					{ orderable: true, className: 'reorder', targets: 10 , 'visible':true},
					{ orderable: true, className: 'reorder', targets: 11 , 'visible':true},
					{ orderable: true, className: 'reorder', targets: 12 , 'visible':true},
					{ orderable: true, className: 'reorder', targets: 13 , 'visible':true},
					{ orderable: true, className: 'reorder', targets: 14 , 'visible':true},
					{ orderable: true, className: 'reorder', targets: 15 , 'visible':true},
					{ orderable: false, className: 'reorder', targets: 16 },
					{ orderable: false, targets: '_all' }
				],
				"bPaginate": false,
				"bLengthChange": false,
				"bFilter": true,
				"bInfo": false,
				"bAutoWidth": false,
				"language": {
						"sEmptyTable":   	"Nenhum dado na tabela",
						"sInfo":         	"_START_ bis _END_ von _TOTAL_ entradas",
						"sInfoEmpty":    	"0 a 0 de 0 entradas",
						"sInfoFiltered": 	"(filtrado por entradas _MAX_)",
						"sInfoPostFix":  	"",
						"sInfoThousands":  	".",
						"sLengthMenu":   	"_MENU_ Mostrar entradas",
						"sLoadingRecords": 	"Carregando...",
						"sProcessing":   	"Aguarde...",
						"sSearch":       	"Pesquisar",
						"sZeroRecords":  	"Nenhuma entrada disponível.",
						"oPaginate": {
							"sFirst":    	"Primeiro",
							"sPrevious": 	"Voltar",
							"sNext":     	"Próximo",
							"sLast":     	"Último"
						},
						"oAria": {
							"sSortAscending":  ": Ativar a ordenação da coluna crescente",
							"sSortDescending": ": Ativar a ordenação da coluna decrescente"
					}
				}});
			
			$('.sure_check').each(function(){
				$(this).click(function(e){
					//e.preventDefault();
					con = confirm("Você tem certeza? Você quer emitir.");
					if(con){
						return true;
					}
					return false;
				});
			});
			$('.cancel_check').each(function(){
				$(this).click(function(e){
					
					con = confirm("Você tem certeza? Você quer cancelar.");
					if(con){
						return true;
					}
					return false;
				});
			});
		});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>SN</th>
					<th>Data e hora do pedido</th>
					<th>Cód da prancha</th>
					<th>Nome da prancha</th>
					<th>Data Horário retirada</th>
					<th>Data prevista devolução</th>
					<th>Data e horário devolução</th>
					<th>Dias atraso</th>
					<th>Status reserva</th>
					<th>Local</th>
					<th>Nome do cliente</th>
					<th>Telefone cliente</th>
					<th>Funcionário retirada</th>
					<th>Obs retirada</th>
					<th>Funcionario devolução</th>
					<th>Obs devolução</th>
					<th>Ações</th>
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					
					<td><?=$sn++;?></td>
					<td><?=$r->ord_start!=null?date("d-m-y",$r->ord_start):"-";?></td>
					<td><?=$r->sur_id;?></td><!--cod da prancha--->
					<td><?=$r->sur_title;?></td><!--nome da prancha--->
					<td><?=$r->date_issued!=null?date("d-m-y H:i:s",$r->date_issued):"-";?></td><!--dt retirada/issued e Horario retirada--->
					<td><?=$r->ord_end!=null?date("d-m-y",$r->ord_end):'-';?></td><!--dt prevista devolução--->
					<td><?=$r->date_completed!=null?date("d-m-y H:i:s",$r->date_completed):'-';?></td><!--dt e hr devolução/ data da tabela status change_date completed--->
					<td><?=$r->status_atual=='pending'||$r->status_atual=='issued'?$r->difDates>0?$r->difDates:'0':"-";?></td><!--dias atraso(se ord_end < change_date/então mostra quantidade de dias)--->
					<td><?=ucfirst($r->status_atual);?></td><!--dstatus reserva--->
					<td><?=$r->lcc_title;?></td><!--localização--->
					<td><?=$r->mem_first?> <?=$r->mem_last?></td><!--nome do membro--->
					<td><?=$r->mem_phone?></td><!--fone do membro--->
					<td><?=$r->func_issued!=null?$r->func_issued:"---";?></td><!--func retirada--->
					<td><?=$r->obs_issued!=null?$r->obs_issued:"---";?></td><!--obs retirada--->
					<td><?=$r->func_completed!=null?$r->func_completed:"---";?></td><!--funcionario devolução--->
					<td><?=$r->obs_completed!=null?$r->obs_completed:"---";?></td><!--obs devolução--->
					<td>
						<a href="javascript:void(0);" myhref="<?=base_url()?>home/product_detail/<?=$r->sur_id;?>/Powerlight-Fish-Performance/json/<?= $r->ord_number.'/'.$r->ord_id?>" goto="<?=base_url(ADMIN."/orders/complete/$r->ord_number"."/"."$r->ord_id");?>" class="btn btn-success ReservePopup">Receber</a> 
					
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>

<!-------------------------------->
<!------template------------->
<!-- Modal -->
<script>
var base_url ='<?=base_url();?>';
$(function(){
	$(".ReservePopup").each(function(){
		$(this).click(function(e){
			e.preventDefault();
			$this = $(this);
			url = $this.attr("myhref");
			
			$.get(url,'',function(obj){
				//console.log(obj);
				//console.log($this.attr("goto"));
				//alert(obj.num_order);
				$(".clearit").remove();
				$("#myModal .sur_title").html(obj.surfboard.sur_title);
				$("#myModal .surfboard_image").attr("src",base_url+"assets/uploads/"+obj.surfboard_image[0].sui_images);
				$("#myModal .sur_brand").html(obj.surfboard.sur_brand);
				$("#myModal .surfboard_location").html(obj.surfboard.lcc_title);
				$("#myModal .surfboard_condition").html(obj.surfboard.sur_condition);
				$("#myModal .sur_volume").html(obj.surfboard.sur_volume);
				$("#myModal .sur_size").html(obj.surfboard.sur_size);
				$("#myModal .location_desc").html(obj.surfboard.lcc_description);
				$("#myModal #ord_number_selecionado").val(obj.num_order);
				$("#myModal #ord_id_selecionado").val(obj.ord_id);
				$("#myModal .getLinkMe").attr("href",$this.attr("goto"));
				for(abc=0; abc<obj.properties.length ; abc++){
					//$(".addProperties").append(obj.properties[abc].sbp_property);
					$(".addProperties").append('<li class="clearit"><strong>'+obj.properties[abc].sbp_property+':</strong> '+obj.properties[abc].sbp_value+'</li>');
				};
				
				//$("#surfBoardReservtion .sur_type").html(obj.surfboard.sur_type);
				//$("#surfBoardReservtion .sur_length").html(obj.surfboard.sur_length);
				//$("#surfBoardReservtion .sur_brand").html(obj.surfboard.sur_brand);
				datez = obj.disabled;
				//console.log(datez);
				var d = new Date();
				d.setMonth(d.getMonth()+1);
				if($(".orderNo").length>0){
					$(".orderNo").html($this.attr("orderNo"));
				}
				if($(".myDates1").length>0){
					dtzs = $this.attr("myDates");
					res = dtzs.split("-");
					
					$(".myDates1").html(res[0]);
					$(".myDates2").html(res[1]);
				}
				
				if($(".opening_time").length>0){
					if(obj.surfboard.lcc_start_time.length==0){
						obj.surfboard.lcc_start_time = 'N/A';
					}
					$(".opening_time").html(obj.surfboard.lcc_start_time);
				}
				if($(".closing_time").length>0){
					if(obj.surfboard.lcc_end_time.length==0){
						obj.surfboard.lcc_end_time = 'N/A';
					}
					$(".closing_time").html(obj.surfboard.lcc_end_time);
				}
				$("#myModal").modal();
			});
		});
	});
});



        
</script>
		<div id="myModal" class="modal fade" role="dialog">

		<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="tableDv">
            <div class="tableCell">
                <div class="contain">
                    <div class="_inner">
					<form role="form" method="post" class="formulario" action="<?= base_url(ADMIN.'/orders/complete/') ?>" id="formulario_issue">
						<div class="crosBtn"></div>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3>Detalhes da prancha a ser recebida</h3>                        
							<hr style="width: 100%"></hr>
							<div class="blockLst" id="proDetail" >
									<div class="contain">
										<br>
										<div class="flexRow flex">
											<div class="col col1" style="width:200px; float:left;">
												<div class="miniSlider">
													<img src=""  alt="" width="200px" class="surfboard_image">
												</div>												
											</div>
											<div class="col col2" style="width:350px; float:left;margin-left:20px;">
												<div class="content ckEditor">
													<h2 class="sur_title"></h2>
													
													<hr>
													<div class="location semi">Localização: <em class="color regular surfboard_location"></em></div>
													
													<hr>
													<!--<h5>Condição: <em class="color surfboard_condition"></em></h5>-->
													
													<ul class="addProperties">
														<li><strong>Volume:</strong> <span class="sur_volume"></span> L </li>
														<!--
														<li><strong>Tamanho:</strong> <span class="sur_size"></span></li>														
														<li><strong>Tipo:</strong> <span class="sur_type"></span></li>						
														<li><strong>Marca:</strong> <span class="sur_size"></span></li>
														-->
													</ul>
													
													<hr>
												</div>
											<div class="form-group">
												<label for="nome_funcionario">Nome do funcionário</label>
												<input type="text" class="form-control" id="nome_funcionario" autofocus name="nome_funcionario">
											</div><!---->
											<div class="form-group">
												<label for="observacao">Observação</label>
												<textarea type="observacao" class="form-control" id="observacao" name="observacao"></textarea>
											</div><!---->
												<input type="hidden" name="ord_number_selecionado" id="ord_number_selecionado" value="" />
												<input type="hidden" name="ord_id_selecionado" id="ord_id_selecionado" value="" />
											</div><!---final dos campos hidden-->
										</div>
									</div>
								
							</div>
						
                            
                            <div class="bTn text-right">
								<button type="button" class="btn btn-lg btn-success" onclick="$('.formulario').submit()">Receber esta prancha agora</button>
                            </div>
						</form>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer" style="clear:both">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>

  </div>
</div><!---fim da modal-->
