
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="tables-main.html">Order</a>
		</li>
		<li class="active">
			<strong><?=$heading;?></strong>
		</li>
	</ol>
	
	<h3><?=$heading;?></h3>
		
		<br><br>
		<script type="text/javascript">
		jQuery( window ).load( function() {
			var $table4 = jQuery( "#table-4" );
			$table4.DataTable({	
				dom: 'Bfrtip',
				buttons: [
					 
					 {
						  extend: 'excel',
						  text: 'Export to Excel',
						  exportOptions: {
								columns: 'th:not(:last-child)'
							}
					 }
				]});
			
			$('.sure_check').each(function(){
				$(this).click(function(e){
					//e.preventDefault();
					con = confirm("Você tem certeza? Você quer emitir.");
					if(con){
						return true;
					}
					return false;
				});
			});
			$('.cancel_check').each(function(){
				$(this).click(function(e){
					
					con = confirm("Você tem certeza? Você quer cancelar.");
					if(con){
						return true;
					}
					return false;
				});
			});
		});
		</script>
		
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					
					<th>SN</th>
					<th>Prancha de surfe</th>
					<th>Localização</th>
					<th>Data de reserva</th>
					<th>Nome do pacote</th>
					<th>Data do pedido</th>
					<th>Status do pedido</th>
					<th>Nome</th>
					<th>O email</th>
					<th>Ações</th>
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $r): ?>
				<tr>
					
					<td><?=$sn++;?></td>
					
					<td><?=$r->sur_title?></td>
					<td><?=$r->lcc_title?></td>
					<td><?=date("d-M-y",$r->ord_start);?> - <?=date("d-M-y",$r->ord_end);?></td>
					<td><?=$r->pkg_title?></td>
					<td><?=date("d-M-y h:i A",$r->ord_time);?></td>
					<td><?=ucfirst($r->ord_status)?></td>
					<td><?=$r->mem_first?> <?=$r->mem_last?></td>
					<td><?=$r->mem_email?></td>
					<td><a href="<?=base_url(ADMIN."/orders/cancel/$r->ord_number/$r->ord_id");?>" class="btn btn-danger cancel_check">Cancelar</a></td>
					
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		