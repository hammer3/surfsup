<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
<div class="row col-md-12">
    
        <div class="col-md-6">
            <h3><i class="fa fa-bars"></i>How it Works Page</h3>
            <hr class="hr-short">
			<div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label"> Main Heading <span class="symbol required"></span></label>
                        <input type="text" name="top_main_heading" value="<?=@$row->top_main_heading?>" class="form-control" required></div>
                    </div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Main Sub Heading  <span class="symbol required"></span></label>
						<textarea name="top_main_sub_heading" class="form-control" required><?=@$row->top_main_sub_heading;?></textarea>
						
					</div>
				</div>
			<h3><i class="fa fa-bars"></i>How it Works Contents</h3>	
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Main Headings  <span class="symbol required"></span></label>
					<input type="text" name="howItMainHeading1" value="<?=@$row->howItMainHeading1?>" class="form-control" required></div>
			</div>
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> First Text  <span class="symbol required"></span></label>
					<input type="text" name="text1" value="<?=@$row->text1?>" class="form-control" required></div>
			</div>
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Picture 1 <span class="symbol required"></span></label>
					<? if(isset($row->picture1)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->picture1;?>" width="150">
					<? endif; ?>
                     <input type="file" name="picture1"  class="form-control" >
                </div>
            </div>	           
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Second Text <span class="symbol required"></span></label>
					<input type="text" name="text2" value="<?=@$row->text2?>" class="form-control" required></div>
			</div>
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Picture 2 <span class="symbol required"></span></label>
					<? if(isset($row->picture2)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->picture2;?>" width="150">
					<? endif; ?>
                     <input type="file" name="picture2"  class="form-control" >
                </div>
            </div>	
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Third Text  <span class="symbol required"></span></label>
					<input type="text" name="text3" value="<?=@$row->text3?>" class="form-control" required></div>
			</div>
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Picture 3 <span class="symbol required"></span></label>
					<? if(isset($row->picture3)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->picture3;?>" width="150">
					<? endif; ?>
                     <input type="file" name="picture3"  class="form-control" >
                </div>
            </div>	 
           
				
        </div>
        <div class="col-md-6">
            <h3><i class="fa fa-bars"></i> Why Use Surfs UP</h3>
            <hr class="hr-short">
            <div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Main Top Heading  <span class="symbol required"></span></label>
					<input type="text" name="howItMainHeading2" value="<?=@$row->howItMainHeading2?>" class="form-control" required></div>
			</div>
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> First Heading  <span class="symbol required"></span></label>
					<input type="text" name="why_heading1" value="<?=@$row->why_heading1?>" class="form-control" required></div>
			</div>
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> First Text  <span class="symbol required"></span></label>
					<input type="text" name="why_text1" value="<?=@$row->why_text1?>" class="form-control" required></div>
			</div>
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Picture 1 <span class="symbol required"></span></label>
					<? if(isset($row->why_picture1)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->why_picture1;?>" width="150">
					<? endif; ?>
                     <input type="file" name="why_picture1"  class="form-control" >
                </div>
            </div>
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Second Heading  <span class="symbol required"></span></label>
					<input type="text" name="why_heading2" value="<?=@$row->why_heading2?>" class="form-control" required></div>
			</div>			
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Second Text <span class="symbol required"></span></label>
					<input type="text" name="why_text2" value="<?=@$row->why_text2?>" class="form-control" required></div>
			</div>
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Picture 2 <span class="symbol required"></span></label>
					<? if(isset($row->why_picture2)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->why_picture2;?>" width="150">
					<? endif; ?>
                     <input type="file" name="why_picture2"  class="form-control" >
                </div>
            </div>
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Third Heading  <span class="symbol required"></span></label>
					<input type="text" name="why_heading3" value="<?=@$row->why_heading3;?>" class="form-control" required></div>
			</div>				
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Third Text  <span class="symbol required"></span></label>
					<input type="text" name="why_text3" value="<?=@$row->why_text3?>" class="form-control" required></div>
			</div>
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Picture 3 <span class="symbol required"></span></label>
					<? if(isset($row->why_picture3)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->why_picture3;?>" width="150">
					<? endif; ?>
                     <input type="file" name="why_picture3"  class="form-control" >
                </div>
            </div>
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Four Heading  <span class="symbol required"></span></label>
					<input type="text" name="why_heading4" value="<?=@$row->why_heading4?>" class="form-control" required></div>
			</div>			
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Four Text  <span class="symbol required"></span></label>
					<input type="text" name="why_text4" value="<?=@$row->why_text4?>" class="form-control" required></div>
			</div>
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Picture 4 <span class="symbol required"></span></label>
					<? if(isset($row->why_picture4)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->why_picture4;?>" width="150">
					<? endif; ?>
                     <input type="file" name="why_picture4"  class="form-control" >
                </div>
            </div>
			<h3><i class="fa fa-bars"></i> END CONTENT</h3>
            <hr class="hr-short">
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Contact Heading</label>
						<input type="text" name="contact_heading" value="<?=@$row->contact_heading?>" class="form-control" required>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Description</label>
						<textarea name="contact_description" class="form-control" required><?=@$row->contact_description;?></textarea>
					</div>
				</div>
               
                    <div class="clearfix"></div>
                    <div class="col-md-12"><hr class="hr-short">
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-green btn-lg" value="Update Settings">
                            </div>
                        </div>
                    </div>
                    <br><br>
                </form>
            </div>            <p>&nbsp;</p>
            <div class="clearfix"></div>
