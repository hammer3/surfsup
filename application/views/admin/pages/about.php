<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
<div class="row col-md-12">
    
        <div class="col-md-6">
            <h3><i class="fa fa-bars"></i>Main Heading</h3>
            <hr class="hr-short">
           

                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label"> Main Heading <span class="symbol required"></span></label>
                        <input type="text" name="top_main_heading" value="<?=@$row->top_main_heading?>" class="form-control" required></div>
                    </div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Main Sub Heading  <span class="symbol required"></span></label>
						<textarea name="top_main_sub_heading" class="form-control" required><?=@$row->top_main_sub_heading;?></textarea>
						
					</div>
				</div>
			
			<h3><i class="fa fa-bars"></i>About US Page</h3>
            <hr class="hr-short">
           

                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label"> Heading <span class="symbol required"></span></label>
                        <input type="text" name="main_heading" value="<?=@$row->main_heading?>" class="form-control" required></div>
                    </div>
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Heading text  <span class="symbol required"></span></label>
						<textarea name="main_heading_sologan" class="form-control" required><?=@$row->main_heading_sologan;?></textarea>
						
					</div>
				</div>
				
				<h3><i class="fa fa-bars"></i> How Are</h3>
				<hr class="hr-short">
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Heading  <span class="symbol required"></span></label>
					<input type="text" name="how_heading" value="<?=@$row->how_heading?>" class="form-control" required></div>
				</div>
				<div class="form-group">
				<div class="col-md-12">
					<label class="control-label"> Heading text</label>
					<input type="text" name="how_detail" value="<?=@$row->how_detail?>" class="form-control" required></div>
				</div>
			<h3><i class="fa fa-bars"></i> Sub Headings AND Text</h3>
            <hr class="hr-short">
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Picture 1 <span class="symbol required"></span></label>
					<? if(isset($row->sub_picture1)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->sub_picture1;?>" width="150">
					<? endif; ?>
                     <input type="file" name="sub_picture1"  class="form-control" >
                </div>
            </div>
           <div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Heading Sub <span class="symbol required"></span></label>
					<input type="text" name="how_sub_heading" value="<?=@$row->how_sub_heading?>" class="form-control" required></div>
				</div>
				<div class="form-group">
				<div class="col-md-12">
					<label class="control-label"> Heading Sub text</label>
					<input type="text" name="how_sub_detail" value="<?=@$row->how_sub_detail?>" class="form-control" required></div>
				</div>
				<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Picture 2 <span class="symbol required"></span></label>
					<? if(isset($row->sub_picture2)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->sub_picture2;?>" width="150">
					<? endif; ?>
                     <input type="file" name="sub_picture2"  class="form-control" >
                </div>
            </div>
           <div class="form-group">
		   
					<div class="col-md-12">
					<label class="control-label"> Heading Sub <span class="symbol required"></span></label>
					<input type="text" name="how_sub_heading1" value="<?=@$row->how_sub_heading1?>" class="form-control" required></div>
				</div>
				<div class="form-group">
				<div class="col-md-12">
					<label class="control-label"> Heading Sub text</label>
					<input type="text" name="how_sub_detail1" value="<?=@$row->how_sub_detail1?>" class="form-control" required></div>
				</div>
				<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Picture 3 <span class="symbol required"></span></label>
					<? if(isset($row->sub_picture3)): ?> 
						<img src="<?=base_url(UPLOAD_PATH."website/").$row->sub_picture3;?>" width="150">
					<? endif; ?>
                     <input type="file" name="sub_picture3"  class="form-control" >
                </div>
            </div>
           <div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Heading Sub  <span class="symbol required"></span></label>
					<input type="text" name="how_sub_heading2" value="<?=@$row->how_sub_heading2?>" class="form-control" required></div>
				</div>
				<div class="form-group">
				<div class="col-md-12">
					<label class="control-label"> Heading Sub text</label>
					<input type="text" name="how_sub_detail2" value="<?=@$row->how_sub_detail2?>" class="form-control" required></div>
				</div>
           
			<div class="form-group">
					<div class="col-md-12">
						<label class="control-label">Detailed Text Centered Row above people  <span class="symbol required"></span></label>
						<textarea name="abovePeopleText" class="form-control" required><?=@$row->abovePeopleText;?></textarea>
						
					</div>
				</div>	
        </div>
        <div class="col-md-6">
            <h3><i class="fa fa-bars"></i> THE PEOPLE </h3>
            <hr class="hr-short">
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> THE PEOPLE Heading  <span class="symbol required"></span></label>
					<input type="text" name="top_people_heading" value="<?=@$row->top_people_heading?>" class="form-control" required>
				</div>
			</div>
			  <hr class="hr-short">
           
           
			
			<h3><i class="fa fa-bars"></i> END CONTENT</h3>
            <hr class="hr-short">
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Contact Heading</label>
						<input type="text" name="contact_heading" value="<?=@$row->contact_heading?>" class="form-control" required>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12">
						<label class="control-label"> Description</label>
						<textarea name="contact_description" class="form-control" required><?=@$row->contact_description;?></textarea>
					</div>
				</div>
               
                    <div class="clearfix"></div>
                    <div class="col-md-12"><hr class="hr-short">
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-green btn-lg" value="Update Settings">
                            </div>
                        </div>
                    </div>
                    <br><br>
                </form>
            </div>            <p>&nbsp;</p>
            <div class="clearfix"></div>
