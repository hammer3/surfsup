
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Location</a>
		</li>
		<li class="active">
			<strong>Add/Edit Slider</strong>
		</li>
	</ol>
	
	<h3>Add/Edit Slider</h3>
	<form role="form" action="" method="post" enctype="multipart/form-data" class="form-horizontal form-groups-bordered">
			
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Title</label>
				
				<div class="col-sm-5">
					<input type="text" name="slider_title" value="<?=@$row->slider_title;?>" class="form-control" id="field-1" placeholder="Title">
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Image</label>
				
				<div class="col-sm-5">
					<input type="file" name="image"  class="form-control" id="field-1" >
				</div>
			</div>
			<? if(isset($row->slider_title)): ?>
			<div class="form-group">
				<img src="<?=base_url(UPLOAD_PATH."website/").$row->slider_image?>" width="200px" >
			</div>
			<? endif;?>
			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" name="slider_status" <?=(@$row->slider_status=='active')?'checked':'';?> />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-8">
					<center>
						<input type="submit"  class="btn btn-success btn-lg" value="Save">
					</center>
				</div>
			</div>
	</form>
