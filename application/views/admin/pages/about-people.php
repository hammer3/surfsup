
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="index.html"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Location</a>
		</li>
		<li class="active">
			<strong>Add/Edit Location</strong>
		</li>
	</ol>
	
	<h3>Add/Edit About Us Peoples</h3>
	<form role="form" action="" method="post" enctype="multipart/form-data" class="form-horizontal form-groups-bordered">
			
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Name</label>
				
				<div class="col-sm-5">
					<input type="text" name="people_name" value="<?=@$row->people_name;?>" class="form-control" id="field-1" placeholder="Name">
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Designation</label>
				
				<div class="col-sm-5">
					<input type="text" name="people_title" value="<?=@$row->people_title;?>" class="form-control" id="field-1" placeholder="Designation">
				</div>
			</div>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Image</label>
				
				<div class="col-sm-5">
					<input type="file" name="image"  class="form-control" id="field-1" >
				</div>
			</div>
			<? if(isset($row->people_image)): ?>
			<div class="form-group">
				<img src="<?=base_url(UPLOAD_PATH."website/").$row->people_image?>" width="200px" >
			</div>
			<? endif;?>
			<div class="form-group">
				<label for="field-1" class="col-sm-3 control-label">Description</label>
				
				<div class="col-sm-5">
					<textarea type="text" name="people_description" class="form-control" id="field-1" placeholder=""><?=@$row->people_description;?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>								
				<div class="col-sm-5">
					<div class="make-switch" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
						<input type="checkbox" name="people_status" <?=(@$row->people_status=='active')?'checked':'';?> />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-8">
					<center>
						<input type="submit"  class="btn btn-success btn-lg" value="Save">
					</center>
				</div>
			</div>
	</form>
