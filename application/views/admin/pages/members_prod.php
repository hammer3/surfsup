
	
	<ol class="breadcrumb bc-3" >
		<li>
			<a href="#"><i class="fa-home"></i>Home</a>
		</li>
		<li>
			<a href="#">Membros</a>
		</li>
		<li class="active">
			<strong><?=$heading;?></strong>
		</li>
	</ol>
	
	<h3>View <?=$heading;?></h3>
		<!--
		<div class="text-right">
			<a href="<?=base_url(ADMIN.'/surfboards/add');?>" class="btn btn-info btn-lg btn-icon icon-left "> <i class="entypo-plus"></i> Add Surfboard</a>
		</div>
		-->
		<br><br>
		<script type="text/javascript">
		
			$(function() {
				var $table4 = jQuery( "#table-4" );
				$table4.DataTable({	
					dom: 'Bfrtip',
					buttons:
					[{
						extend: 'excel',
						text: 'Exportar para Excel',
						exportOptions: {
							columns: 'th:not(:last-child)'
						}
					}],
					rowReorder: {
						selector: '.myreorder'
					},
					reorder:true,

					columnDefs: 
					[
						{ orderable: true, className: 'reorder', targets: 0 },
						{ orderable: true, className: 'reorder', targets: 1 },
						{ orderable: true, className: 'reorder', targets: 2 },
						{ orderable: true, className: 'reorder', targets: 3 },
						{ orderable: true, className: 'reorder', targets: 4 },
						{ orderable: true, className: 'reorder', targets: 5 },
						{ orderable: true, className: 'reorder', targets: 6 },
						{ orderable: false, targets: '_all' }
					],
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": true,
					"bInfo": false,
					"bAutoWidth": false,
					"language": {
							"sEmptyTable":   	"Nenhum dado na tabela",
							"sInfo":         	"_START_ bis _END_ von _TOTAL_ entradas",
							"sInfoEmpty":    	"0 a 0 de 0 entradas",
							"sInfoFiltered": 	"(filtrado por entradas _MAX_)",
							"sInfoPostFix":  	"",
							"sInfoThousands":  	".",
							"sLengthMenu":   	"_MENU_ Mostrar entradas",
							"sLoadingRecords": 	"Carregando...",
							"sProcessing":   	"Aguarde...",
							"sSearch":       	"Pesquisar",
							"sZeroRecords":  	"Nenhuma entrada disponível.",
							"oPaginate": {
								"sFirst":    	"Primeiro",
								"sPrevious": 	"Voltar",
								"sNext":     	"Próximo",
								"sLast":     	"Último"
							},
							"oAria": {
								"sSortAscending":  ": Ativar a ordenação da coluna crescente",
								"sSortDescending": ": Ativar a ordenação da coluna decrescente"
						}
					}

					});
			});
		</script>
		<? if($this->session->flashdata('message_success')): ?>
			<div class="alert alert-success">
				<strong>Success.</strong> <?=$this->session->flashdata('message_success');?>
			</div>

		<? endif; ?>
		<? if($this->session->flashdata('message_error')): ?>
			<div class="alert alert-danger">
				<strong>Error.</strong> <?=$this->session->flashdata('message_error');?>
			</div>

		<? endif; ?>
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
					<th>SN</th>
					<th>Dt Cadastro</th>
					<th>Nome completo</th>
					<th>Email</th>					
					<th>Telefone</th>
					<th>Endereço</th>
					<th>Plano</th>
					<th>Ações</th>
				</tr>
			</thead>
			
			<tbody>
			<? $sn=1 ;?>
			<? foreach($row->result() as $col): ?>
				<tr>

					<td class="myreorder">
						<?=$sn++;?>
					</td>
					<td><?=$col->mem_created_time;?></td>
					<td><?=$col->mem_first;?> <?=$col->mem_last;?> </td>
					<td><?=$col->mem_email;?></td>
					<td><?=$col->mem_phone;?></td>
					<td><?=$col->mem_address." - ".$col->mem_city;?></td>
					<td><?=$col->package;?></td>
					<td><a href="<?=base_url(ADMIN."/members/viewDetail/".$col->mem_id);?>" class="btn btn-info">Visualizar detalhes</a></td>					
				</tr>
			<? endforeach; ?>	
			</tbody>
		</table>
		
