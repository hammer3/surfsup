<div class="row col-md-12">
    <form role="form" class="form-horizontal" action="" method="post">
        <div class="col-md-6">
            <h3><i class="fa fa-bars"></i> Meta Tags</h3>
            <hr class="hr-short">
			<div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"> Head Tag for Analytic or Verifiation etc Just paste the meta tag <span class="symbol required"></span></label>
                            <textarea rows="5" name="web_header" class="form-control"><?=@$row->web_header?></textarea>
                        </div>
                    </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Meta Description <span class="symbol required"></span></label>
                    <input type="text" name="site_meta_desc" value="<?=@$row->site_meta_desc?>" class="form-control" required></div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label"> Meta Keywords <span class="symbol required"></span></label>
                        <input type="text" name="site_meta_keyword" value="<?=@$row->site_meta_keyword?>" class="form-control" required></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"> Meta Copyright <span class="symbol required"></span></label>
                            <input type="text" name="site_meta_copyright" value="<?=@$row->site_meta_copyright?>" class="form-control" required></div>
                        </div>
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Meta Author <span class="symbol required"></span></label>
					<input type="text" name="site_meta_author" value="<?=@$row->site_meta_author?>" class="form-control" required></div>
				</div>
				<h3><i class="fa fa-bars"></i> Social Media</h3>
				<hr class="hr-short">
				<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> Facebook Link</label>
					<input type="text" name="site_facebook" value="<?=@$row->site_facebook?>" class="form-control" required></div>
				</div>
				<div class="form-group">
				<div class="col-md-12">
					<label class="control-label"> Twitter Link</label>
					<input type="text" name="site_twitter" value="<?=@$row->site_twitter?>" class="form-control" required></div>
				</div>
           
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Instagram Link</label>
                    <input type="text" name="site_instagram" value="<?=@$row->site_instagram?>" class="form-control" required></div>
                </div>
            
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Youtube Link</label>
                    <input type="text" name="site_youtube" value="<?=@$row->site_youtube?>" class="form-control" required>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h3><i class="fa fa-bars"></i> General Detail</h3>
            <hr class="hr-short">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Site Domain <span class="symbol required"></span></label>
                    <input type="text" name="site_domain" value="<?=@$row->site_domain?>" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Site Name <span class="symbol required"></span></label>
                    <input type="text" name="site_name" value="<?=@$row->site_name?>" class="form-control" required>
                </div>
            </div>
           
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> Site Email <span class="symbol required"></span></label>
                    <input type="text" name="site_email" value="<?=@$row->site_email?>" class="form-control" required></div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="control-label"> Site No-Reply Email <span class="symbol required"></span></label>
                        <input type="text" name="site_noreply_email" value="<?=@$row->site_noreply_email?>" class="form-control" required></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"> Site Phone <span class="symbol required"></span></label>
                            <input type="text" name="site_phone" value="<?=@$row->site_phone?>"  class="form-control" required>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"> Address <span class="symbol required"></span></label>
                            <textarea rows="5" name="site_address" class="form-control"><?=@$row->site_address?></textarea>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"> Footer text <span class="symbol required"></span></label>
                            <textarea rows="5" name="footer_text" class="form-control"><?=@$row->footer_text?></textarea>
                        </div>
                    </div>
					
                </div>
            <div class="col-md-12">
				<div class="row">
				<h3><i class="fa fa-bars"></i> Email  Template</h3>
					<div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"> Email Subject <span class="symbol required"></span></label>
                            <input type="text" name="email_subject" value="<?=@$row->email_subject?>"  class="form-control" required>
                        </div>
                    </div>
					<div class="form-group">
						<div class="col-md-12">
							<label class="control-label " style="color:red"> Note: for Name use <big>{firstname}</big> , <big>{lastname}</big> and  <big>{link}</big>  <span class="symbol required"></span></label>
							
							 <textarea name="email_template" width="100%" style="height:900px" class="form-control ckeditor"  ><?=@$row->email_template?></textarea>
						</div>
					</div>
				</div>
			</div>
                    <div class="clearfix"></div>
                    <div class="col-md-12"><hr class="hr-short">
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-green btn-lg" value="Update Settings">
                            </div>
                        </div>
                    </div>
                    <br><br>
                </form>
            </div>            <p>&nbsp;</p>
            <div class="clearfix"></div>
