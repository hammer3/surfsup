<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
<div class="row col-md-12">
    
 
            <h3><i class="fa fa-bars"></i>Privacy and policy</h3>
            <hr class="hr-short">
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> PP Heading <span class="symbol required"></span></label>
					<input type="text" name="pp_heading" value="<?=@$row->pp_heading?>" class="form-control" required></div>
			</div>
			
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> PP <span class="symbol required"></span></label>
					
                     <textarea name="pp_detail" width="100%" style="height:900px" class="form-control ckeditor"  ><?=@$row->pp_detail?></textarea>
                </div>
            </div>
               
                    <div class="clearfix"></div>
                    <div class="col-md-12"><hr class="hr-short">
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-green btn-lg" value="Update Settings">
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"> Policy Reservation <span class="symbol required"></span></label>
                            <textarea rows="5" name="policy_reservation_text" class="form-control ckeditor"><?=@$row->policy_reservation_text?></textarea>
                        </div>
                    </div>
                    <br><br>
                </form>
            </div>            <p>&nbsp;</p>
            <div class="clearfix"></div>
