<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
<div class="row col-md-12">
    
 
            <h3><i class="fa fa-bars"></i>Competition Terms and Conditions</h3>
            <hr class="hr-short">
			<div class="form-group">
					<div class="col-md-12">
					<label class="control-label"> ToS Heading <span class="symbol required"></span></label>
					<input type="text" name="tos_heading" value="<?=@$row->tos_heading?>" class="form-control" required></div>
			</div>
			
			<div class="form-group">
                <div class="col-md-12">
                    <label class="control-label"> TOS <span class="symbol required"></span></label>
					
                     <textarea name="tos_detail" width="100%" style="height:900px" class="form-control ckeditor"  ><?=@$row->tos_detail?></textarea>
                </div>
            </div>
               
                    <div class="clearfix"></div>
                    <div class="col-md-12"><hr class="hr-short">
                        <div class="form-group text-right">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-green btn-lg" value="Update Settings">
                            </div>
                        </div>
                    </div>
                    <br><br>
                </form>
            </div>            <p>&nbsp;</p>
            <div class="clearfix"></div>
