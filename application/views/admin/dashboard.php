
	<div class="row">

			<div class="col-sm-6 col-xs-6">
		
				<div class="tile-stats tile-red">
					<div class="icon"><i class="entypo-users"></i></div>
					<div class="num" data-start="0" data-end="<?=count($count_members);?>" data-postfix="" data-duration="1500" data-delay="0"><?=count($count_members);?></div>
		
					<h3>Registered users</h3>
					<p>On website.</p>
				</div>
		
			</div>
		
			
			<div class="clear visible-xs"></div>
		
			<div class="col-sm-6 col-xs-6">
		
				<div class="tile-stats tile-aqua">
					<div class="icon"><i class="entypo-mail"></i></div>
					<div class="num" data-start="0" data-end="<?=count($count_contact);?>" data-postfix="" data-duration="1500" data-delay="1200"><?=count($count_contact);?></div>
		
					<h3>Contact Us Messages</h3>
					<p>Total Contact Us Message</p>
				</div>
		
			</div>
			
			<div class="clear visible-xs"></div>
		
			<div class="col-sm-4 col-xs-6">
		
				<div class="tile-stats tile-aqua">
					<div class="icon"><i class="entypo-news"></i></div>
					<div class="num" data-start="0" data-end="<?=count($count_issued);?>" data-postfix="" data-duration="1500" data-delay="1200"><?=count($count_issued);?></div>
		
					<h3>Total Issued Surfboard</h3>
					<p>Total Issued Surfboard</p>
				</div>
		
			</div>		
			<div class="clear visible-xs"></div>
		
			<div class="col-sm-4 col-xs-6">
		
				<div class="tile-stats tile-aqua">
					<div class="icon"><i class="entypo-news"></i></div>
					<div class="num" data-start="0" data-end="<?=count($count_delayed);?>" data-postfix="" data-duration="1500" data-delay="1200"><?=count($count_delayed);?></div>
		
					<h3>Delayed Surfboard</h3>
					<p>Total Delayed Surfboard</p>
				</div>
		
			</div>
			
			<div class="clear visible-xs"></div>
		
			<div class="col-sm-4 col-xs-6">
		
				<div class="tile-stats tile-aqua">
					<div class="icon"><i class="entypo-news"></i></div>
					<div class="num" data-start="0" data-end="<?=count($count_today);?>" data-postfix="" data-duration="1500" data-delay="1200"><?=count($count_today);?></div>
		
					<h3>Today Surfboards</h3>
					<p>Today Issued,Pending &amp; Canceled</p>
				</div>
		
			</div>
			
		</div>
		
		<br />


