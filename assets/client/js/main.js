$(window).on('load', function() {
	$(".btn-group.bootstrap-select.show-tick.txtBox.open span.filter-option.pull-left").html('Nenhum Selecionado');
	$(".btn-group.bootstrap-select.show-tick.txtBox span.filter-option.pull-left").html('Nenhum Selecionado');

	$('#owl-surfs').owlCarousel({
		// dots: false,
		loop: true,
		autoplay: true,
		margin: 10,
		smartSpeed: 1000,
		autoplayTimeout: 8000,
		autoplayHoverPause: true,
		responsive: {
			0:{
				items: 1,
				center: true,
				margin: 0,
				autoWidth: true
			},
			480:{
				items: 2
			},
			991:{
				items: 3
			},
			1200:{
				items: 4
			}
		}
	});
	$('#owl-navigation').owlCarousel({
		autoWidth: true,
		dots: false,
		items: 1
	});
});
$(window).on('load', function() {
	$('#lightSlider').lightSlider({
		gallery: true,
		item: 1,
		auto: true,
		loop: true,
		speed: 2500,
		pause: 8000,
		slideMargin: 0,
		// vertical: true,
		enableDrag: false,
		thumbMargin: 4,
		thumbItem: 4,
		currentPagerPosition:'center',
		onSliderLoad: function(el) {
			el.lightGallery({
				selector: '#lightSlider .lslide',
				hideBarsDelay: 1000
			});
		}  
	});
	'use strict';
	$('.miniSlider').delay(300).css('opacity', '1');
});
$(window).on('load', function() {
	$('.selectpicker').selectpicker();
});	
$(document).ready(function () {
	$('.datepickerz').datepicker({
		
		multidate: true,
		format: 'mm-dd-yyyy',
		todayHighlight: true,

		datesDisabled: datesForDisable,
		startDate: new Date()  ,
		multidateSeparator: ',  ',
		templates : {
			leftArrow: '<i class="fi-arrow-left"></i>',
			rightArrow: '<i class="fi-arrow-right"></i>'
		}
	});
	var d = new Date();
	d.setMonth(d.getMonth()+1);
	//Range Datepicker//
	if($('.datepicker').length>0){
		$('.datepicker').daterangepicker({
			todayHighlight: false,
			
			
			//	"applyLabel": "Aplicar",
			//	"cancelLabel": "Cancelar",
			//	"fromLabel": "DE",
			//	"toLabel": "HASTA"
			
			minDate: new Date()  ,
			maxDate: d,
			isInvalidDate: function(date) {				
				var formatted = date.format('DD/MM/YYYY');
				return datesForDisable.indexOf(formatted) > -1;				
			},			
			locale: {
			    format: 'DD/MM/YYYY',
				applyLabel: "Aplicar",
				cancelLabel: "Cancelar",
				fromLabel: "DE",
				toLabel: "Para"
			},
			
		});
	}
	
});
$(function(){
	$(".confirmCancel").click(function(e){
		result = confirm("Você tem certeza? Essa ação não pode ser desfeita.");
		if(result){
			return true;
		}
		return false;
		//e.preventDefault();
		
	});
	$(".ReservePopup").each(function(){
		$(this).click(function(e){
			e.preventDefault();
			$this = $(this).parent('tr');
			if(!($this.length>0)){
				$this = $(this);
			}
			url = $this.attr("myhref");
			
			$.get(url,'',function(obj){
				$("#surfBoardReservtion .sur_title").html(obj.surfboard.sur_title);
				$("#surfBoardReservtion .surfboard_image").attr("src",base_url+"assets/uploads/"+obj.surfboard_image[0].sui_images);
				$("#surfBoardReservtion .sur_brand").html(obj.surfboard.sur_brand);
				$("#surfBoardReservtion .surfboard_location").html(obj.surfboard.lcc_title);
				$("#surfBoardReservtion .surfboard_condition").html(obj.surfboard.sur_condition);
				$("#surfBoardReservtion .sur_volume").html(obj.surfboard.sur_volume);
				$("#surfBoardReservtion .sur_size").html(obj.surfboard.sur_size);
				$("#surfBoardReservtion .location_desc").html(obj.surfboard.lcc_description);
				//$("#surfBoardReservtion .sur_type").html(obj.surfboard.sur_type);
				//$("#surfBoardReservtion .sur_length").html(obj.surfboard.sur_length);
				//$("#surfBoardReservtion .sur_brand").html(obj.surfboard.sur_brand);
				datez = obj.disabled;
				//console.log(datez);
				var d = new Date();
				d.setMonth(d.getMonth()+1);
				if($(".orderNo").length>0){
					$(".orderNo").html($this.attr("orderNo"));
				}
				if($(".myDates1").length>0){
					dtzs = $this.attr("myDates");
					res = dtzs.split("-");
					
					$(".myDates1").html(res[0]);
					$(".myDates2").html(res[1]);
				}
				
				if($(".opening_time").length>0){
					if(obj.surfboard.lcc_start_time.length==0){
						obj.surfboard.lcc_start_time = 'N/A';
					}
					$(".opening_time").html(obj.surfboard.lcc_start_time);
				}
				if($(".closing_time").length>0){
					if(obj.surfboard.lcc_end_time.length==0){
						obj.surfboard.lcc_end_time = 'N/A';
					}
					$(".closing_time").html(obj.surfboard.lcc_end_time);
				}
				if($('.datepicker2').length>0){
					$('.datepicker2').daterangepicker({
						
						minDate: new Date()  ,
						maxDate: d,
						isInvalidDate: function(date) {				
							var formatted = date.format('DD/MM/YYYY');
							return datez.indexOf(formatted) > -1;				
						},			
						locale: {
							format: 'DD/MM/YYYY',
							applyLabel: "Aplicar",
							cancelLabel: "Cancelar",
							fromLabel: "DE",
							toLabel: "Para",
							todayHighlight: false,
						},
						
					});
				}
				$("#surfBoardReservtion").show();
			});
		});
	});
	
});

$(document).ready(function() {

	
    /*_____ Toggle _____*/
    $(document).on('click', '.toggle', function() {
        $('.toggle').toggleClass('active');
        $('nav').toggleClass('active');
    });

    w = $(window).width();
    if (w <= 991) {
        $(document).on('click', '#nav > li.drop > a', function(e) {
            e.preventDefault();
            $('.sub').not($(this).parent().children('.sub').slideToggle()).slideUp();
        });
    }
    $(window).on('resize', function(){
        $('#nav > li > .sub').removeAttr('style');
    });


    /*_____ Upload File _____*/
    var imgFile;
    $(document).on('click', '.uploadImg', function() {
        imgFile = $(this).attr('data-image-src');
        $(this).parents('form').find('.uploadFile').trigger('click');
    });
    $(document).on('change', '.uploadFile', function() {
        // alert(imgFile);
        var file = $(this).val();
		fileSize = (($(this)[0].files[0].size)/1024)/1024;
		console.log(fileSize);
		var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
			return false;
        }
		if(fileSize>1){
			alert("File Size should be less than 1MB. ");
			return false;
		}
		readURL(this);
        //$('.uploadImg').html(file);
    });
	function readURL(input) {

	  if (input.files && input.files[0]) {
		  var reader = new FileReader();
		reader.onload = function(e) {
		  $('.uploadImg').html('<img src="'+e.target.result+'">');
		}
		reader.readAsDataURL(input.files[0]);
	  }
	}
/*
$("#pictoget").change(function() {
		fileSize = (($(this)[0].files[0].size)/1024)/1024;
		console.log(fileSize);
		var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            bootbox.alert("Only formats are allowed : "+fileExtension.join(', '));
			return false;
        }
		if(fileSize>2){
			bootbox.alert("File Size should be less than 2MB. ");
			return false;
		}
		readURL(this);
		$("#buttonToshow").html('<button class="btn btn-info remove-picture">Remove Image</button>').picRemove();
		
	});
*/

    /*_____ Drop Down _____*/
    $(document).on('click', '.dropBtn', function(e) {
        e.stopPropagation();
        var $this = $(this).parent().children('.dropCnt');
        $('.dropCnt').not($this).removeClass('active');
        var $parent = $(this).parent('.dropDown');
        $parent.children('.dropCnt').toggleClass('active');
    });
    $(document).on('click', '.dropCnt', function(e) {
        e.stopPropagation();
    });
    $(document).on('click', function() {
        $('.dropCnt').removeClass('active');
    });


    /*_____ FAQ's _____*/
    $(document).on('click', '.faqLst > li > h3', function() {
        $('.faqLst > li .cntnt').not($(this).parent('li').children('.cntnt').slideToggle()).slideUp();
        $(".faqLst > li i").not($(this).parent('li').children('i').toggleClass("fi-plus").toggleClass("fi-minus")).removeClass("fi-minus").addClass("fi-plus");
    });


    /*_____ Popup _____*/
    $(document).on('click', '.popup', function(e) {
        if ($(e.target).closest('.popup ._inner, .popup .inside').length === 0) {
            $('.popup').fadeOut('3000');
            $('body').removeClass('flow');
            $('.popup .videoBlk').html('');
            $('#loadVideo').html('');
        }
    });
    $(document).on('click', '.crosBtn', function() {
        $('.popup').fadeOut();
        $('body').removeClass('flow');
        $('.popup .videoBlk').html('');
        $('#loadVideo').html('');
    });
    $(document).keydown(function(e) {
        if (e.keyCode == 27) $('.crosBtn').click();
    });
    $(document).on('click', '.popBtn', function() {
        var popUp = $(this).data('popup');
        $('body').addClass('flow');
        $('.popup[data-popup= ' + popUp + ']').fadeIn();
    });

});


$(window).on('load', function() {
    $('img').parent('a').css('display', 'block');
    $('.loader').delay(700).fadeOut();
    $('#pageloader').delay(1200).fadeOut('slow');
});

$(function(){
	$('.datepicker').val('');
});
